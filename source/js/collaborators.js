function generateRCTable(data, names) {

  if (data.message !== undefined) {
    $('#collaborators tbody').append('<div class="panel panel-warning">' +
                             '<div class="panel-heading">' + data.message + '</div></div>');
    return;
  }

  if (data.rc_info !== undefined) {
    var name_set =  {};
    $.each(data.rc_info, function(_, affl) {
      var affl_row = $('<tr>');

      var full_name = buildFullName(affl);
      name_set[full_name] = true; // add user to set
      var rc_text = getRCText(affl);
      var rc_id = getRCID(affl).toString();
      var rc_link = "one_rc_complete.html?rc_id=" + rc_id;
      var email = textBlank(affl.email_primary) ? "" : affl.email_primary;
      var url_primary = textBlank(affl.url_primary) ? "" : affl.url_primary;

      affl_row.append(generateCell(affl.affiliation));
      affl_row.append(generateCell(full_name));
      affl_row.append(generateCell(affl.geog_area));
      affl_row.append(generateCell(rc_text, rc_link));
      affl_row.append(generateCell(email, 'mailto'));
      affl_row.append(generateCell(url_primary, url_primary));
      affl_row.append(generateCell(affl.status_defn.startsWith("Active") ? "" : affl.status_defn));

      $('#collaborators tbody').append(affl_row);
    });

    var emptyType = $.fn.dataTable.absoluteOrder( [
      { value: '', position: 'bottom' }
    ]);

    var table = $('#collaborators').DataTable({
      paging:false,
      fixedHeader: {header:true, footer:false},
      scrollY:400,
      searching:false,
      order: [[0, 'asc']],
      info:false,
      columnDefs: [
        { type: emptyType, targets: 0 },
        { type: 'any-number', targets: 3 }
      ]
    });

    $('#num_people').replaceWith($('<p>')
      .text("Total number of people actively affiliated with GLIMS: " + Object.keys(name_set).length))
  }
}

var spinner = new Spinner(spinnerOpts).spin(document.getElementById('rc_info'));

var collaborators = function(names){
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/rc_info',
    success: function(data, textStatus, jqXHR) {
      generateRCTable(data, names);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/rc_info Service Failed: ' + textStatus);
      spinner.stop();
    }
  });
};

var placenames = function(){
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/displaynames',
    success: function(data, textStatus, jqXHR) {
      collaborators(data);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/displaynames Service Failed: ' + textStatus);
      spinner.stop();
    }
  });
};

placenames();
