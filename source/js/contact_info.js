function buildAffilDept(info) {
  var ad = "";
  if (!textBlank(info.affiliation)) {
    ad = info.affiliation;
  }
  if (!textBlank(info.department)) {
    if (ad !== "") ad = ad + ", ";
    ad = ad + info.department;
  }

  return ad;
}

function generateTableData(data) {
  if (data.message !== undefined) {
    $('contacts tbody').append('<div class="panel panel-warning">' +
                              '<div class="panel-heading">' + data.message + '</div></div>');
    return;
  }

  if (data.contactinfo !== undefined) {
    $.each(data.contactinfo, function(_, contact) {
      var contact_row = $('<tr>');

      var full_name = buildFullName(contact);
      var affilDept = buildAffilDept(contact);
      var address = buildAddress(contact);
      var email = textBlank(contact.email_primary) ? "" : contact.email_primary;

      contact_row.append(generateCell(full_name));
      contact_row.append(generateCell(contact.glims_role));
      contact_row.append(generateCell(affilDept));
      contact_row.append(generateCell(address));
      contact_row.append(generateCell(contact.workphone_primary));
      contact_row.append(generateCell(email, 'mailto'));

      $('#contacts tbody').append(contact_row);
    });

    var table = $('#contacts').DataTable({
      paging:false,
      fixedHeader: {header:true, footer:false},
      scrollY:2000,
      searching:false,
      order: [],
      info:false,
      scrollCollapse: true
    });
  }
}

var spinner = new Spinner(spinnerOpts).spin(document.getElementById('rc_info'));

var contacts = function() {
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/contactinfo',
    success: function(data, textStatus, jqXHR) {
      generateTableData(data);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/contactinfo Service Failed: ' + textStatus);
      spinner.stop();
    }
  });
};

contacts();
