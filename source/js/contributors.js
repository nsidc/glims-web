var textsearch_link_base = "http://glims.colorado.edu/textsearch/search_results.php"
  + "?glacier_id=glac_id&glacier_name=glac_name&src_date=src_date"
  + "&db_area=db_area&wgms_id=wgms_id&anlys_id=anlys_id&mean_elev=mean_elev"
  + "&release_okay_date=release_okay_date&affliation=affliation"
  + "&center="
  + "&glac_name_text=&country_name_select=Select..."
  + "&glac_area_min=&glac_area_max=&glac_id_text=&wgms_id_text="
  + "&prime_class_select=Select..."
  + "&glac_width_min=&glac_width_max="
  + "&glac_length_min=&glac_length_max=&glac_elev_min=&glac_elev_max="
  + "&rc_box=true&rc_name_select=";

function buildAnalystText(analysts) {
  var text = "";
  $.each(analysts, function(_, analyst) {
    if (text !== "") {
      text = text + "<br />";
    }
    text = text + buildFullName(analyst);
  });

  return text;
}

function generateTableData(data) {
  if (data.message !== undefined) {
    $('contributors tbody').append('<div class="panel panel-warning">' +
                              '<div class="panel-heading">' + data.message + '</div></div>');
    return;
  }

  if (data.contributors !== undefined) {
    $.each(data.contributors, function(_, contributor) {
      var contributor_row = $('<tr>');

      var full_name = buildFullName(contributor.submitter);
      var analysts = buildAnalystText(contributor.analysts);
      var rc_link = textsearch_link_base + contributor.rc_id;

      contributor_row.append(generateCell(full_name));
      contributor_row.append(generateCell(analysts));
      contributor_row.append(generateCell(contributor.rc_id, rc_link));
      contributor_row.append(generateCell(contributor.geog_area));
      contributor_row.append(generateCell(contributor.packages));
      contributor_row.append(generateCell(contributor.outlines));

      $('#contributors tbody').append(contributor_row);
    });

    var table = $('#contributors').DataTable({
      paging:false,
      fixedHeader: {header:true, footer:false},
      scrollY:400,
      searching:false,
      order: [],
      info:false,
      scrollCollapse: true
    });
  }
}

var spinner = new Spinner(spinnerOpts).spin(document.getElementById('rc_info'));

var contributors = function() {
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/contributors',
    success: function(data, textStatus, jqXHR) {
      generateTableData(data);
      $("#rc_info").css("visibility", "hidden");
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/contributors Service Failed: ' + textStatus);
      $("#rc_info").css("visibility", "hidden");
      spinner.stop();
    }
  });
};

contributors();
