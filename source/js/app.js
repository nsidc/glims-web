
// Map Selection Handler

map.on('singleclick', function (e) {
  $('#mapSelectionDetails .modal-body').empty();

  // get a list of all possible visible layer names
  var lyrNames = [];
  var viewResolution = map.getView().getResolution();
  ol.control.LayerSwitcher.forEachRecursive(map, function (lyr) {
    var lyrType = lyr.get('type') || lyr.get('mytype');
    if ($.inArray(lyrType, ['base', 'group']) === -1) {
      if (lyr.getVisible()) {
        lyrNames.push(lyr.getSource().getParams().LAYERS);
      }
    }
  });

  var lyrNamesStr = lyrNames.toString();

  var getFeatureInfoParams = {
    'INFO_FORMAT':  'application/json',
    'QUERY_LAYERS': lyrNamesStr,
    'LAYERS': lyrNamesStr,
    'FEATURE_COUNT': 20
  };

  var url = fogSource.getGetFeatureInfoUrl(e.coordinate, viewResolution,
    'EPSG:3857', getFeatureInfoParams);
  // call with second arg of 0 at this level
  get_feature_info_robustly(url, 0);

});

// Recursive function to detect and handle redirects
function get_feature_info_robustly(url, level) {
  $.ajax({
    type: 'get',
    dataType: 'text',
    url: url,
    success: loadQueryTables,
    error: function (jqxhr, textStatus, error, level) {
      if (jqxhr.status == 302 && level == 0) {
        // call with second arg of 1 to break infinite loop
        get_feature_info_robustly(jqxhr.getResponseHeader("Location"), 1);
      }
      else {
        $.notify('Data Retrieval Failed', 'error');
      }
    }
  }
  );
}

function normalize_geojson(data) {

  var mapServerLegacyResponse = {
    "Alaska": [],
    "ArcticCanadaNorth": [],
    "ArcticCanadaSouth": [],
    "AntarcticSubantarctic": [],
    "CaucasusMiddleEast": [],
    "CentralAsia": [],
    "CentralEurope": [],
    "Countries": [],
    "dcw_glaciers": [],
    "glac_lines": [],
    "glacier_photos": [],
    "glathida_points": [],
    "extinct_glaciers_view": [],
    "FOG_points": [],
    "glims_points": [],
    "GreenlandPeriphery": [],
    "Iceland": [],
    "LowLatitudes": [],
    "NewZealand": [],
    "NorthAsia": [],
    "RC_Outlines": [],
    "RC_Points": [],
    "Scandinavia": [],
    "SouthernAndes": [],
    "SouthAsiaEast": [],
    "RussianArctic": [],
    "SouthAsiaWest": [],
    "Svalbard": [],
    "WesternCanadaUS": [],
    "RGI2000-v7.0-G-01_alaska_epsg3857": [],
    "RGI2000-v7.0-G-02_western_canada_usa_epsg3857": [],
    "RGI2000-v7.0-G-03_arctic_canada_north_epsg3857": [],
    "RGI2000-v7.0-G-04_arctic_canada_south_epsg3857": [],
    "RGI2000-v7.0-G-05_greenland_periphery_epsg3857": [],
    "RGI2000-v7.0-G-06_iceland_epsg3857": [],
    "RGI2000-v7.0-G-07_svalbard_jan_mayen_epsg3857": [],
    "RGI2000-v7.0-G-08_scandinavia_epsg3857": [],
    "RGI2000-v7.0-G-09_russian_arctic_epsg3857": [],
    "RGI2000-v7.0-G-10_north_asia_epsg3857": [],
    "RGI2000-v7.0-G-11_central_europe_epsg3857": [],
    "RGI2000-v7.0-G-12_caucasus_middle_east_epsg3857": [],
    "RGI2000-v7.0-G-13_central_asia_epsg3857": [],
    "RGI2000-v7.0-G-14_south_asia_west_epsg3857": [],
    "RGI2000-v7.0-G-15_south_asia_east_epsg3857": [],
    "RGI2000-v7.0-G-16_low_latitudes_epsg3857": [],
    "RGI2000-v7.0-G-17_southern_andes_epsg3857": [],
    "RGI2000-v7.0-G-18_new_zealand_epsg3857": [],
    "RGI2000-v7.0-G-19_subantarctic_antarctic_islands_epsg3857": [],
    "WGI_points": []
  }
  data.features.forEach(function (feature, index) {
    // Remove the ".NNNN" from the end of the layer name
    var layerName = feature.id.split(".").slice(0, -1).join(".");
    switch (layerName) {
      case "GLIMS_Points":
        feature.properties.src_date = feature.properties.src_date.replace('Z', '');
        feature.properties.release_date = feature.properties.release_date.replace('Z', '');
        mapServerLegacyResponse.glims_points.push(feature.properties);
        break;
      case "GLIMS_Glacier_Outlines":
        feature.properties.src_date = feature.properties.src_date.replace('Z', '');
        feature.properties.release_date = feature.properties.release_date.replace('Z', '');
        mapServerLegacyResponse.glac_lines.push(feature.properties);
        break;
      case "WGI_points":
        mapServerLegacyResponse.WGI_points.push(feature.properties);
        break;
      case "FOG_points":
        mapServerLegacyResponse.FOG_points.push(feature.properties);
        break;
      case "glathida_points":
        mapServerLegacyResponse.glathida_points.push(feature.properties);
        break;
      case "extinct_glaciers_view":
        mapServerLegacyResponse.extinct_glaciers_view.push(feature.properties);
        break;
      case "glacier_photos":
        if (feature.properties.photo_year === null) {
          feature.properties.photo_year = '';
        } else {
          feature.properties.photo_year = feature.properties.photo_year.toString();
        }
        if (feature.properties.photo_month === null) {
          feature.properties.photo_month = '';
        } else {
          feature.properties.photo_month = '-' + feature.properties.photo_month.toString().padStart(2, '0');
        }
        if (feature.properties.photo_day === null) {
          feature.properties.photo_day = '';
        } else {
          feature.properties.photo_day = '-' + feature.properties.photo_day.toString().padStart(2, '0');
        }

        feature.properties.photo_date = feature.properties.photo_year +
          feature.properties.photo_month + feature.properties.photo_day;
        mapServerLegacyResponse.glacier_photos.push(feature.properties);
        break;
      case "dcw_glaciers":
        mapServerLegacyResponse.dcw_glaciers.push(feature.properties);
        break;
      case "GLIMS_RC_Outlines":
        mapServerLegacyResponse.RC_Outlines.push(feature.properties);
        break;
      case "GLIMS_RC_Points":
        mapServerLegacyResponse.RC_Points.push(feature.properties);
        break;
      case 'RGI_Alaska':
        mapServerLegacyResponse.Alaska.push(feature.properties);
        break;
      case "RGI_AntarcticSubantarctic":
        mapServerLegacyResponse.AntarcticSubantarctic.push(feature.properties);
        break;
      case "RGI_ArcticCanadaNorth":
        mapServerLegacyResponse.ArcticCanadaNorth.push(feature.properties);
        break;
      case "RGI_ArcticCanadaSouth":
        mapServerLegacyResponse.ArcticCanadaSouth.push(feature.properties);
        break;
      case 'RGI_CaucasusMiddleEast':
        mapServerLegacyResponse.CaucasusMiddleEast.push(feature.properties);
        break;
      case "RGI_CentralAsia":
        mapServerLegacyResponse.CentralAsia.push(feature.properties);
        break;
      case "RGI_CentralEurope":
        mapServerLegacyResponse.CentralEurope.push(feature.properties);
        break;
      case "RGI_GreenlandPeriphery":
        mapServerLegacyResponse.GreenlandPeriphery.push(feature.properties);
        break;
      case "RGI_Iceland":
        mapServerLegacyResponse.Iceland.push(feature.properties);
        break;
      case "RGI_LowLatitudes":
        mapServerLegacyResponse.LowLatitudes.push(feature.properties);
        break;
      case "RGI_NewZealand":
        mapServerLegacyResponse.NewZealand.push(feature.properties);
        break;
      case "RGI_NorthAsia":
        mapServerLegacyResponse.NorthAsia.push(feature.properties);
        break;
      case "RGI_RussianArctic":
        mapServerLegacyResponse.RussianArctic.push(feature.properties);
        break;
      case "RGI_Scandinavia":
        mapServerLegacyResponse.Scandinavia.push(feature.properties);
        break;
      case "RGI_SouthAsiaEast":
        mapServerLegacyResponse.SouthAsiaEast.push(feature.properties);
        break;
      case "RGI_SouthAsiaWest":
        mapServerLegacyResponse.SouthAsiaWest.push(feature.properties);
        break;
      case "RGI_SouthernAndes":
        mapServerLegacyResponse.SouthernAndes.push(feature.properties);
        break;
      case "RGI_Svalbard":
        mapServerLegacyResponse.Svalbard.push(feature.properties);
        break;
      case "RGI_WesternCanadaUS":
        mapServerLegacyResponse.WesternCanadaUS.push(feature.properties);
        break;
      case "RGI2000-v7.0-G-01_alaska_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-01_alaska_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-02_western_canada_usa_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-02_western_canada_usa_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-03_arctic_canada_north_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-03_arctic_canada_north_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-04_arctic_canada_south_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-04_arctic_canada_south_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-05_greenland_periphery_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-05_greenland_periphery_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-06_iceland_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-06_iceland_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-07_svalbard_jan_mayen_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-07_svalbard_jan_mayen_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-08_scandinavia_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-08_scandinavia_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-09_russian_arctic_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-09_russian_arctic_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-10_north_asia_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-10_north_asia_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-11_central_europe_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-11_central_europe_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-12_caucasus_middle_east_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-12_caucasus_middle_east_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-13_central_asia_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-13_central_asia_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-14_south_asia_west_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-14_south_asia_west_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-15_south_asia_east_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-15_south_asia_east_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-16_low_latitudes_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-16_low_latitudes_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-17_southern_andes_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-17_southern_andes_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-18_new_zealand_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-18_new_zealand_epsg3857"].push(feature.properties);
        break;
      case "RGI2000-v7.0-G-19_subantarctic_antarctic_islands_epsg3857":
        mapServerLegacyResponse["RGI2000-v7.0-G-19_subantarctic_antarctic_islands_epsg3857"].push(feature.properties);
        break;
      default:
        console.log(feature.id);
        console.log(layerName);
    }
  });
  return mapServerLegacyResponse;
}


// Callback for map query

var loadQueryTables = function(data) {
  data = normalize_geojson(JSON.parse(data));

  if (!Object.keys(data).find(function (key) {return !$.isEmptyObject(data[key]);})) {
    $.notify('No Results Found', 'warn');
    return;
  }

  // Add glac_ids as string to data object for download button to use
  if (!$.isEmptyObject(data['glac_lines'])) {
    glac_ids = []
    $.each(data['glac_lines'], function (i, glac_obj) {
      if (!$.isEmptyObject(glac_obj)) {
        glac_ids.push(glac_obj['glac_id'])
      }
    });
    data.glacIdStr = glac_ids.join();
  }

  if (!$.isEmptyObject(data['glims_points'])) {
    glac_ids = []
    $.each(data['glims_points'], function (i, glac_obj) {
      if (!$.isEmptyObject(glac_obj)) {
        glac_ids.push(glac_obj['glac_id'])
      }
    });
    data.glacIdStr = glac_ids.join();
  }

  // combine RGI layer data into single list
  // (dirty hack since mapserver template not using layer group)
  var rgi6LayersAll = [
    'Alaska',
    'AntarcticSubantarctic',
    'ArcticCanadaNorth',
    'ArcticCanadaSouth',
    'CaucasusMiddleEast',
    'CentralAsia',
    'CentralEurope',
    'GreenlandPeriphery',
    'Iceland',
    'LowLatitudes',
    'NewZealand',
    'NorthAsia',
    'RussianArctic',
    'Scandinavia',
    'SouthAsiaEast',
    'SouthAsiaWest',
    'SouthernAndes',
    'Svalbard',
    'WesternCanadaUS'
  ];
  var rgi6Layers = rgi6LayersAll.filter(function (el) {
    return Object.keys(data).indexOf(el) != -1;
  });
  if (rgi6Layers.length > 0) {
    rgi_data = [];
    for (var i = 0; i < rgi6Layers.length; i++) {
      if (data[rgi6Layers[i]] !== undefined) {
        rgi_data = rgi_data.concat(data[rgi6Layers[i]]);
      }
    }
    data.rgi6 = rgi_data;
  }

  // combine RGI70 layer data into single list
  var rgi7LayersAll = [
    "RGI2000-v7.0-G-01_alaska_epsg3857",
    "RGI2000-v7.0-G-02_western_canada_usa_epsg3857",
    "RGI2000-v7.0-G-03_arctic_canada_north_epsg3857",
    "RGI2000-v7.0-G-04_arctic_canada_south_epsg3857",
    "RGI2000-v7.0-G-05_greenland_periphery_epsg3857",
    "RGI2000-v7.0-G-06_iceland_epsg3857",
    "RGI2000-v7.0-G-07_svalbard_jan_mayen_epsg3857",
    "RGI2000-v7.0-G-08_scandinavia_epsg3857",
    "RGI2000-v7.0-G-09_russian_arctic_epsg3857",
    "RGI2000-v7.0-G-10_north_asia_epsg3857",
    "RGI2000-v7.0-G-11_central_europe_epsg3857",
    "RGI2000-v7.0-G-12_caucasus_middle_east_epsg3857",
    "RGI2000-v7.0-G-13_central_asia_epsg3857",
    "RGI2000-v7.0-G-14_south_asia_west_epsg3857",
    "RGI2000-v7.0-G-15_south_asia_east_epsg3857",
    "RGI2000-v7.0-G-16_low_latitudes_epsg3857",
    "RGI2000-v7.0-G-17_southern_andes_epsg3857",
    "RGI2000-v7.0-G-18_new_zealand_epsg3857",
    "RGI2000-v7.0-G-19_subantarctic_antarctic_islands_epsg3857",
  ];
  var rgi7Layers = rgi7LayersAll.filter(function (el) {
    return Object.keys(data).indexOf(el) != -1;
  });
  if (rgi7Layers.length > 0) {
    rgi7_data = [];
    for (var i = 0; i < rgi7Layers.length; i++) {
      if (data[rgi7Layers[i]] !== undefined) {
        rgi7_data = rgi7_data.concat(data[rgi7Layers[i]]);
      }
    }
    data.rgi7 = rgi7_data;
  }

  // Modify float precision
  if (!$.isEmptyObject(data['WGI_points'])) {
    $.each(data['WGI_points'], function (i, wgi_obj) {
      if (!$.isEmptyObject(wgi_obj)) {
        if (!$.isEmptyObject(wgi_obj['total_area'])) {
          wgi_obj['total_area'] = parseFloat(wgi_obj['total_area']).toFixed(2);
        }
        if (!$.isEmptyObject(wgi_obj['mean_length'])) {
          wgi_obj['mean_length'] = parseFloat(wgi_obj['mean_length']).toFixed(2);
        }
      }
    })
  }

  if (!$.isEmptyObject(data['rgi6'])) {
    $.each(data['rgi6'], function (i, rgi_obj) {
      if (!$.isEmptyObject(rgi_obj)) {
        if (!$.isEmptyObject(rgi_obj['AREA'])) {
          rgi_obj['AREA'] = parseFloat(rgi_obj['AREA']).toFixed(2);
        }
      }
    })
  }

  if (!$.isEmptyObject(data['rgi7'])) {
    $.each(data['rgi7'], function (i, rgi7_obj) {
      if (!$.isEmptyObject(rgi7_obj)) {
        rgi7_obj['area_km2'] = parseFloat(rgi7_obj['area_km2']).toFixed(3);
        if (!$.isEmptyObject(rgi7_obj['src_date'])) {
          rgi7_obj['src_date'] = rgi7_obj['src_date'].replace('T00:00:00', '');
        }
      }
    })
  }

  if (!$.isEmptyObject(data['glac_lines'])) {
    $.each(data['glac_lines'], function (i, glac_obj) {
      if (!$.isEmptyObject(glac_obj)) {
        if (!$.isEmptyObject(glac_obj['db_area'])) {
          glac_obj['db_area'] = parseFloat(glac_obj['db_area']).toFixed(2);
        }
      }
    })
  }

  if (!$.isEmptyObject(data['glims_points'])) {
    $.each(data['glims_points'], function (i, glac_obj) {
      if (!$.isEmptyObject(glac_obj)) {
        if (!$.isEmptyObject(glac_obj['db_area'])) {
          glac_obj['db_area'] = parseFloat(glac_obj['db_area']).toFixed(2);
        }
      }
    })
  }

  // Get rid of pesk 'Z' from end of gone_date
  if (!$.isEmptyObject(data['extinct_glaciers_view'])) {
    $.each(data['extinct_glaciers_view'], function (i, ext_obj) {
      if (!$.isEmptyObject(ext_obj)) {
        if (!$.isEmptyObject(ext_obj['gone_date'])) {
          ext_obj['gone_date'] = ext_obj['gone_date'].slice(0, -1);
        }
      }
    })
  }

  // Variables needed by Handlebars template
  data.gtng = gtng;
  data.getDataUrl = getDataUrl;

  $('#mapSelectionDetails .modal-body').html(Handlebars.templates.layer_query_tables(data));
  $("#mapSelectionDetails").modal('show');
};

// Search Submission Handler

var renderPlacename = function (data) {
  if (data.totalResultsCount === 0) {
    var notifyOpts = {position: 'bottom', className: 'warn', autoHideDelay: 2000};
    $('#placename-input').notify('No Results Found', notifyOpts);
  } else {
    var lng = parseFloat(data.geonames[0].lng);
    var lat = parseFloat(data.geonames[0].lat);
    map.setView(
      new ol.View({
        center: ol.proj.transform([lng, lat], 'EPSG:4326', 'EPSG:3857'),
        zoom: 12
      })
    );
    $('#placename-input').val('');
  }
};

// Search Form Handler

$('#placename-search').on('submit', function (e) {
  e.preventDefault();
  var spinner = new Spinner(spinnerOpts).spin(document.getElementById('wrapper'));
  $.ajax({
    url: 'https://secure.geonames.org/search',
    dataType: 'json',
    data: {type: 'json', username: 'kpurdon', name: $('#placename-input').val()},
    success: function (data, textStatus, jqXHR) {
      renderPlacename(data);
      spinner.stop();
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $.notify('Search Failed');
      spinner.stop();
    }
  });
});

$('#download-current').click(function (e) {
  // cleanup when replaced with Python service
  var currentExtent = map.getView().calculateExtent(map.getSize());
  var latLonExtentMin = ol.proj.transform(currentExtent.slice(0, 2), 'EPSG:3857', 'EPSG:4326');
  var latLonExtentMax = ol.proj.transform(currentExtent.slice(2), 'EPSG:3857', 'EPSG:4326');
  var extent_array = [latLonExtentMin[0], latLonExtentMin[1], latLonExtentMax[0], latLonExtentMax[1]];
  var extent_clause = 'clause=' + extent_array.join();
  var params = 'download_type=extent&' + extent_clause;
  window.open(getDataUrl + params, '_blank');
});

$('#download-all').click(function(e) {
  window.open(getAllDataUrl, '_blank');
});
