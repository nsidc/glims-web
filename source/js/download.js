$('#clipnship').submit(function (e) {
    e.preventDefault();




    var download_type = $.url('?download_type');
    var clause = $.url('?clause');
    if (!download_type) {
        var notifyOpts = {className: 'error', autoHide: false};
        $.notify('Parameter ?download_type=[id|extent] Undefined', notifyOpts);
    }
    if (!clause) {
        var notifyOpts = {className: 'error', autoHide: false};
        $.notify('Parameter ?clause=[id list|bbox(w,s,e,n)] Undefined', notifyOpts);
    }

    var form_vars = $('#clipnship').serialize();
    var querystring = 'download_type=' + download_type + '&clause=' + clause + '&' + form_vars;
    var prepare_url = serviceURL + '/downloadprepare?' + querystring;

    $.ajax({
        url: prepare_url,

        dataType: "json",

        complete: function (jqXHR, textStatus) {
            console.log(jqXHR);
            console.log(textStatus);
        },

        error: function (jqXHR, error) {
            if (jqXHR.status === 200 && error === "parsererror") {
                alert("bingo!");
            }
            console.log(jqXHR);
            console.log(textStatus);
        },

        // prepare url successfully gave us a response with the filename to
        // expect, so now let's ask if it's ready until it is
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            console.log(jqXHR);
            console.log(textStatus);
        }
    });

    var form_values = {}
    $.each($('#clipnship :input').serializeArray(), function (i, field) {
        form_values[field.name] = field.value
    });
    var format_name = form_values.file_format + ' ' + form_values.archive_type

    $('#submit').prop('disabled', true);
    $('#submit').val('Creating ' + format_name + '...');

    $('#download_progress_message').empty();

    var spinner = new Spinner({
        opacity: 0.25,
        lines: 15,
        length: 10,
        width: 1,
        scale: 1,
        position: 'relative',
        top: '15px',
        left: '25px',
    }).spin(document.getElementById('download_progress_spinner'));

    var stopSpinner = function () {
        spinner.stop();
        $('#submit').prop('disabled', false);
        $('#submit').val('Download Data');
    };

    // http://stackoverflow.com/a/33414145
    var wait = function (ms) {
        var start = new Date().getTime();
        var end = start;
        while (end < start + ms) {
            end = new Date().getTime();
        }
    };

    var error = function (jqXHR, textStatus, errorThrown) {
        stopSpinner();
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        $('#download_progress_message').html('An error was encountered while ' +
            'generating the file.');
    };

    $.ajax({
        url: prepare_url,

        dataType: "json",

        complete: function (jqXHR, textStatus) {
            console.log(jqXHR);
            console.log(textStatus);
        },

        error: error,

        // prepare url successfully gave us a response with the filename to
        // expect, so now let's ask if it's ready until it is
        success: function (data, textStatus, jqXHR) {
            var filename = data.filename;
            console.log(filename);

            var fileready_url = serviceURL + '/downloadfileready?filename=' + filename;

            var download = function (maxTimeout) {
                var finished_successfully = false;

                // we subtract time on each attempt, so when this is called with
                // negative time, we have timed out
                if (maxTimeout < 0) {
                    return error(jqXHR, "timed out");
                }

                // only see if the file is ready every 5 seconds
                var waitFor = 5000;
                wait(waitFor);

                $.ajax({
                    url: fileready_url,
                    crossDomain: true,
                    headers: {
                        "accept": "application/json",
                        "Access-Control-Allow-Origin": "*"
                    },
                    error: error,
                    success: function (data, textStatus, jqXHR) {
                        var ready = data.ready;

                        // try again, a few seconds closer to timing out
                        if (!ready) {
                            return download(maxTimeout - waitFor)
                        }

                        // create an invisible link and click it download the
                        // file
                        var filename = data.filename;
                        console.log(filename);

                        var file_url = serviceURL + '/downloadfile?filename=' + filename;

                        var a_tag = "<a style='display:none' href='" + file_url + "' download>" + "Download link" + "</a>";
                        $('#svc_url_div').html(a_tag);
                        $('#svc_url_div a')[0].click();
                        $('#svc_url_div a').remove();

                        var file_download_link = "<a href='" + file_url + "' download>" + "this link" + "</a>";
                        $('#download_progress_message').html('Your ' + format_name + ''
                            + ' should be downloaded. You'
                            + ' can download it again by using '
                            + file_download_link +
                            ' (will expire in 15 minutes).');
                        finished_successfully = true;
                        stopSpinner();
                    }
                });
            };

            download(1000 * 60 * 120);
        }
    });
});
