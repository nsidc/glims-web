// Environment Configuration (This variable is modified by Puppet!)

var environment = 'integration';

// URL for various parts
var mapservURL;
var serviceURL;

var protocol = 'https:';

var host = window.location.hostname;

if (environment == 'dev') {
  var user = 'UNDEFINED';
  mapservURL = 'https://integration.glims.org/geoserver/GLIMS/wms';
  serviceURL = protocol + '//dev.glims-services.' + user + '.dev.int.nsidc.org';
} else {
  // Base URL Configuration
  var URLPrefix = protocol + '//' + ((environment === 'production' ||
    environment === 'blue' ||
    environment === 'green') ? 'www' : environment) + '.glims.org';
  mapservURL = URLPrefix + '/geoserver/GLIMS/wms';
  serviceURL = URLPrefix + '/services';
}

if (host === '127.0.0.1' || host === 'localhost') {
  serviceURL = 'http://' + host + ':8080';
  // You'll have to manually accept the self signed cert
  // mapservURL = 'https://localhost/geoserver/GLIMS/wms';

  mapservURL = 'https://integration.glims.org/geoserver/GLIMS/wms';
}

// Map Request Options

var getlegend = '?version=1.3.0&format=image/png&request=GetLegendGraphic&height=20&service=WMS&LEGEND_OPTIONS=forceLabels%3Aon%3BfontAntiAliasing%3Atrue';

// Spinner Options

var spinnerOpts = {opacity: 0.25, lines: 15, length: 10, width: 1, scale: 3};

var getDataUrl = 'download.html?';
var getAllDataUrl = 'https://daacdata.apps.nsidc.org/pub/DATASETS/nsidc0272_GLIMS_v1/';
