function generateTableData(data) {
  if (data.message !== undefined) {
    $('collaborators tbody').append('<div class="panel panel-warning">' +
                              '<div class="panel-heading">' + data.message + '</div></div>');
    return;
  }

  if (data.rc_info !== undefined) {
    var info = data.rc_info['rc_id'];
    if (data.rc_info['geog_area'] !== undefined && data.rc_info['geog_area'] !== null) {
      info = info + ' for ' + data.rc_info['geog_area'];
    }
    $('#rc_info_head').text(info);
    $(document).attr('title', $(document).attr('title') + ' ' + info);

    $.each(data.rc_info, function(key, val) {
      if (key === 'affiliated') {
        $.each(data.rc_info['affiliated'], function(_, affl) {
          var affl_row = $('<tr>');

          var full_name = buildFullName(affl);
          var url_primary = textBlank(affl.url_primary) ? "" : affl.url_primary;
          var email = textBlank(affl.email_primary) ? "" : affl.email_primary;
          var status = ""
          if (affl.p_status > 1) {
            status = affl.status_defn;
          } else if (affl.rcp_status > 1) {
            status = "Inactive in this RC";
          }

          affl_row.append(generateCell(affl.contact_id));
          affl_row.append(generateCell(full_name));
          affl_row.append(generateCell(affl.affiliation));
          affl_row.append(generateCell(url_primary, url_primary));
          affl_row.append(generateCell(email, 'mailto'));
          affl_row.append(generateCell(status));

          $('#collaborators tbody').append(affl_row);
        });

        var table = $('#collaborators').DataTable({
          paging:false,
          fixedHeader: {header:true, footer:false},
          scrollY:400,
          searching:false,
          order: [],
          info:false,
          scrollCollapse: true
        });
      } else {
        value_cell = $("#"+key);
        if (value_cell.length !== 0) {
          value_cell.html(val);
        }
      }
    });
  }
}

var spinner = new Spinner(spinnerOpts).spin(document.getElementById('rc_info'));

var affiliates = function() {
  params = getParams();
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/rc_info/' + params['rc_id'],
    success: function(data, textStatus, jqXHR) {
      generateTableData(data);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/rc_info/<ID> Service Failed: ' + textStatus);
      spinner.stop();
    }
  });
};

affiliates();
