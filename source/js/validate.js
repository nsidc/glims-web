

$.ajaxSetup({
  crossDomain: true,
  xhrFields: {
    withCredentials: true
  }
});


var auth_url = serviceURL + '/authenticated';

$.ajax({
  url: auth_url,
  crossDomain: true,
  cache: false,
  dataType: "json",

  error: function (jqXHR, error) {
    if (jqXHR.status === 200 && error === "parsererror") {

      $("#preauthorized").html(`<p>A free NASA Earthdata Login
    account is required to access these data (<a target="_blank" href="https://nsidc.org/the-drift/data-update/data-set-access-update-global-land-ice-measurements-from-space-glims-glacier-database-version-1/">More info</a>).<br/><a target='_self' href='${prepare_url}' >Login to Earthdata</a></p>`)
      $("#authorized").html("")
    }
    console.log(jqXHR);
    console.log(textStatus);
  },

  // prepare url successfully gave us a response with the filename to
  // expect, so now let's ask if it's ready until it is
  success: function (data, textStatus, jqXHR) {
    if (data.authenticated === false) {
      $("#preauthorized").html(`<p>A free NASA Earthdata Login
    account is required to access these data (<a target="_blank" href="https://nsidc.org/the-drift/data-update/data-set-access-update-global-land-ice-measurements-from-space-glims-glacier-database-version-1/">More info</a>).<br/><a referrerpolicy='unsafe-url'  href='${serviceURL}/auth'>Login to Earthdata</a></p>`)
      $("#authorized").html("")
    } else {
      $("#preauthorized").html("<p>You are logged in to Earthdata.  See choices and download button below.</p>")
      $("#authorized").html("<input id='submit' name='submit' type='submit' value='Download Data'>")
    }
    console.log(data);
    console.log(jqXHR);
    console.log(textStatus);
  }
});
