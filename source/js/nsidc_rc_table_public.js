id_sort_fn = function(a, b) {
  var aa = parseInt(a, 10);
  var bb = parseInt(b, 10);

  if (aa < bb) {
    return -1;
  } else if (aa > bb) {
    return 1;
  } else {
    return 0;
  }
}

function addMainRC(rc_data, info) {
  var rc_id = info.rc_id;
  var stewards = {};

  if (rc_data[rc_id] !== undefined) {
    stewards = rc_data[rc_id].stewards;
  }

  rc_data[rc_id] = info;
  rc_data[rc_id].stewards = stewards;
}

function addStewardRC(rc_data, info) {
  var rc_id = info.rc_id;
  var parent_id = info.center;

  if (rc_data[parent_id] === undefined) {
    rc_data[parent_id] = {};
    rc_data[parent_id].stewards = {};
  }
  rc_data[parent_id].stewards[rc_id] = info;
}

function parseInfo(data) {
  var rc_data = {};
  var coord_data = {};

  $.each(data.rc_info, function(_, info) {
    if (info.chief !== info.contact_id) {
      // We only want the chief affiliate
      return;
    }

    var rc_id = info.rc_id;

    if (rc_id < 500 || rc_id > 699) {
      addMainRC(rc_data, info);
    } else {
      if (info.rc_status === 1 && rc_id > 599) {
        // Only active, and ID within 600 to 699
        coord_data[info.rc_id] = info;
      }
      // Steward RC
      if (info.rc_status > 1 || info.center === undefined || info.center === null) {
        // Inactive, ignore
        return;
      }
      addStewardRC(rc_data, info);
    }
  });

  return {'rc_data': rc_data, 'coord_data': coord_data};
}

function generateRCStewardTable(data) {
  if (data.message !== undefined) {
    $('#rc_steward_table tbody').append('<div class="panel panel-warning">' +
                                 '<div class="panel-heading">' + data.message + '</div></div>');
    return;
  }

  if (data.rc_info !== undefined) {
    var center_data = parseInfo(data);
    var rc_data = center_data['rc_data'];
    var coord_data = center_data['coord_data'];
  } else {
    $('#rc_steard_table tbody').replaceWith('<div class="panel panel-warning">' +
                                '<div class="panel-heading">No Records Available</div></div>');
  }

  $.each(Object.keys(rc_data).sort(id_sort_fn), function(_, rc_id) {
    if (rc_id === null) return;

    rc_id = parseInt(rc_id, 10);
    var rc_row = $('<tr>');
    rc_row.addClass('rc_header');

    var info = rc_data[rc_id];
    var full_name = buildFullName(info);
    var email = textBlank(info.email_primary) ? "" : "mailto:" + info.email_primary;
    var url_primary = 'one_rc_complete.html?rc_id='+rc_id;

    var id_cell = generateCell(info.rc_id);
    if (info.glacier_outline_data === true) {
      id_cell.addClass('glacier_outline');
    }

    rc_row.append(id_cell);
    rc_row.append(generateCell(info.affiliation, url_primary));
    rc_row.append(generateCell(info.geog_area));
    rc_row.append(generateCell(full_name, email));

    $('#rc_steward_table tbody').append(rc_row);

    var stewards = rc_data[rc_id].stewards;
    $.each(Object.keys(stewards).sort(id_sort_fn), function(_, stew_id) {
      if (stew_id === null) return;

      var rc_row = $('<tr>');

      stew_id = parseInt(stew_id, 10);
      var stew_row = $('<tr>');

      var info = stewards[stew_id];
      var full_name = buildFullName(info);
      var email = textBlank(info.email_primary) ? "" : "mailto:" + info.email_primary;
      var url_primary = 'one_rc_complete.html?rc_id='+stew_id;

      var steward_id_name = $('<td>');
      var steward_div = $('<div>');
      steward_div.addClass('steward');
      steward_id_div = $('<div>').html(stew_id);
      steward_id_div.addClass('id_cell');
      if (info.glacier_outline_data === true) {
        steward_id_div.addClass('glacier_outline');
      }
      var text = info.affiliation;
      text = textBlank(text) ? '' : text.link(url_primary);
      var steward_name_div = $('<div>').html(text);;
      steward_name_div.addClass('name_cell');
      steward_div.append(steward_id_div);
      steward_div.append(steward_name_div);

      rc_row.append(generateCell(''));
      rc_row.append($('<td>').append(steward_div));
      rc_row.append(generateCell(info.geog_area));
      rc_row.append(generateCell(full_name, email));

      $('#rc_steward_table tbody').append(rc_row);
    });
  });

  var table = $("#rc_steward_table").DataTable({
    paging:false,
    fixedHeader: {header:true, footer:false},
    scrollY:400,
    searching:false,
    ordering:false,
    info:false,
    scrollCollapse:true
  });

  // Coordination Center
  $.each(Object.keys(coord_data).sort(id_sort_fn), function(_, coord_id) {
    if (coord_id === null) return;

    coord_id = parseInt(coord_id, 10);
    var coord_row = $('<tr>');

    var info = coord_data[coord_id];
    var full_name = buildFullName(info);
    var email = textBlank(info.email_primary) ? "" : "mailto:" + info.email_primary;
    var url_primary = 'one_rc_complete.html?rc_id='+coord_id;

    coord_row.append(generateCell(info.rc_id));
    coord_row.append(generateCell(info.affiliation, url_primary));
    coord_row.append(generateCell(full_name, email));

    $('#coordination_table tbody').append(coord_row);
  });

  var table = $("#coordination_table").DataTable({
    paging:false,
    fixedHeader: {header:true, footer:false},
    scrollY:400,
    searching:false,
    ordering:false,
    info:false,
    scrollCollapse:true
  });
}

var spinner = new Spinner(spinnerOpts).spin(document.getElementById('rc_info'));

var rc_steward_data = function() {
  $.ajax({
    dataType: 'json',
    url: serviceURL + '/rc_info',
    success: function(data, textStatus, jqXHR) {
      generateRCStewardTable(data);
      spinner.stop();
    },
    error: function(jqXHR, textStatus, errorThrown) {
      $.notify('/rc_info Service Failed: ' + textStatus);
      spinner.stop();
    }
  })
}

rc_steward_data();
