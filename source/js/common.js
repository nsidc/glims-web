// affl needs "rc_id" and "center" properties
function useParentRC(affl) {
  return affl.rc_id >= 500 && affl.center !== null && affl.center <= 500
}

// affl needs "rc_id" and "center" properties
function getRCText(affl) {
  if (useParentRC(affl)) {
    return "Steward for RC " + affl.center;
  } else {
    return "RC " + affl.rc_id;
  }
}

// affl needs "rc_id" and "center" properties
function getRCID(affl) {
  if (useParentRC(affl)) {
    return affl.center;
  } else {
    return affl.rc_id;
  }
}

// If link is defined, it will add it, otherwise just the text
function generateCell(text, link) {
  if (link === "mailto") {
    link = textBlank(text) ? "" : 'mailto:' + text;
  }
  if (!textBlank(link) && !textBlank(text)) {
    // Ensure it's a string, and not a number
    text = "" + text;
    text = text.link(link);
  }
  return $('<td>').html(text);
}

// info should have "surname" and "givennames" properties
// Will return in SURNAME, GIVENNAMES format, unless one of the two is missing
function buildFullName(info) {
  var full_name = "";
  if (!textBlank(info.surname)) {
    full_name = info.surname;
  }
  if (!textBlank(info.givennames)) {
    if (full_name !== "") full_name = full_name + ", ";
    full_name = full_name + info.givennames;
  }

  return full_name;
}

function buildAddress(info) {
  var address = ""
  if (!textBlank(info.address1)) {
    address = info.address1;
  }
  if (!textBlank(info.city)) {
    if (address !== "") address = address + " ";
    address = address + info.city;
  }
  if (!textBlank(info.state_province)) {
    if (address !== "") address = address + ", ";
    address = address + info.state_province;
  }
  if (!textBlank(info.country_code)) {
    if (address !== "") address = address + " ";
    address = address + info.country_code;
  }
  if (!textBlank(info.postal_code)) {
    if (address !== "") address = address + " ";
    address = address + info.postal_code;
  }
  return address;
}

function textBlank(text) {
  return text === undefined || text === null || text === '';
}

function getParams() {
  var idx = document.URL.indexOf('?');
  var params = {};
  var pairs = document.URL.split('?').pop().split('&');

  for (var i = 0, p; i < pairs.length; i++) {
    p = pairs[i].split('=');
    params[p[0]] = p[1];
  }

  return params;
}
