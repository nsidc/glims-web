function openSearchSection() {
    $('#search-form').slideDown(150);
    $('#search-toggle-img').attr('src', '../images/triangle_open.png');
    $('.pagination-style').hide();
    $('.dataTables_wrapper').hide();
    $('#results_count').val('100');
    $('#results_offset').val('0');

}

function closeSearchSection() {
    $('#search-form').slideUp(150);
    $('#search-toggle-img').attr('src', '../images/triangle_closed.png');
    $('.pagination-style').show();
    $('.dataTables_wrapper').show();
}


$('#searchform').submit(function(e) {
  e.preventDefault();

  $('#error-msg').hide();
  $('#error-msg').text('');
  $('.dataTables_wrapper').hide();

  var form_vars = $('#searchform :input')
      .filter(function(index, element) {
        return $(element).val() != '';
      }).serialize();

  if (form_vars === '') {
    $('#error-msg').show();
    $('#error-msg').text('You must enter at least one parameter to search!');
    return;
  }
  var query_url = serviceURL + '/search?' + form_vars;

  $('#submit').prop('disabled', true);
  $('#submit').val('Searching...');
  closeSearchSection();

  $('#search_progress_message').empty();

  var spinner = new Spinner({
    opacity: 0.25,
    lines: 15,
    length: 25,
    width: 10,
    radius: 30,
    scale: 1,
    corners: 1,
    position: 'relative',
    top: '105px',
    left: '65px',
    color: '#ff0000',
    fadeColor: 'transparent'
  }).spin(document.getElementById('search_progress_spinner'));

  var stopSpinner = function () {
    spinner.stop();
    $('#submit').prop('disabled', false);
    $('#submit').val('Search');
  };

  // http://stackoverflow.com/a/33414145
  var wait = function(ms) {
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
        end = new Date().getTime();
    }
  };

  var error = function (jqXHR, textStatus, errorThrown) {
    stopSpinner();
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
    $('#search_progress_message').html('An error occurred while searching.');
  };

  //alert(query_url);

  $.ajax({
    url: query_url,

    dataType: "json",

    complete: function (jqXHR, textStatus) {
      console.log(jqXHR);
      console.log(textStatus);
    },

    error: error,

    // prepare url successfully gave us a response with the filename to
    // expect, so now let's ask if it's ready until it is
    success: function (data, textStatus, jqXHR) {
      generateTableData(data);
      stopSpinner();
    }
  });
});

function paginationDisplay(rsInfo) {
  $('#results_count').val(100);
  $('#results_offset').val(rsInfo.offset);
  $('#results_total').val(rsInfo.total_count);
  var link = '../download.html?download_type=token&clause=' + rsInfo.download_token
  $("#download-link").attr("href", link)

  if (rsInfo.total_count > 0) {
    var currentPage = Math.round(rsInfo.offset /  rsInfo.page_size ) + 1;
    var totalPages = Math.round(rsInfo.total_count / rsInfo.page_size);
    var text = '< Page ' + currentPage + ' of ' + totalPages + ' > Total Results: ' + rsInfo.total_count;
  } else {
    text = 'No results found';
  }
  $('#pagination-display').text(text);
}

function generateTableData(data) {
  if (data.message !== undefined) {
    $('results tbody').append('<div class="panel panel-warning">' +
                          '<div class="panel-heading">' + data.message + '</div></div>');
    return;
  }

  table.clear();

  if (data.results !== undefined) {
    $.each(data.results, function(i, result) {
      var result_row = $('<tr>');

      var info = '<a href="../info.html?anlys_id=' + result.analysis_id + '" target="_blank"><img src="../images/info.png" /></a>';
      var download = '<a href="../download.html?download_type=id&clause=' + result.glacier_id + '" target="_blank"><img src="../images/download.png" /></a>';

      result_row.append(generateCell(result.glacier_id));
      result_row.append(generateCell(result.glacier_name));
      result_row.append(generateCell(result.analysis_id));
      result_row.append(generateCell(result.glacier_calculated_area));
      result_row.append(generateCell(result.source_date));
      result_row.append(generateCell(result.available_date));
      result_row.append(generateCell(result.analyst_surname));
      result_row.append(generateCell(result.analyst_affiliation));
      result_row.append(generateCell(info + download));

      // $('#results tbody').append(result_row);
      table.row.add(result_row);
    });
    // paginationDisplay(data.results[0]);
    paginationDisplay(data);
    $('.dataTables_wrapper').show();
  }
  table.draw();
}

var countries_success = function(data, textStatus, jqXHR) {
  $.each(data, function(index, value) {
    var option = new Option(value, value);
    $('#country_name').append($(option));
  });
}

var rc_list_success = function(data, textStatus, jqXHR) {
  $.each(data, function(index, value) {
    var option = new Option(value['rc_id'] + ' - ' + value['affiliation'] + ' (' + value['geog_area'] + ')', value['rc_id']);
    $('#rc_id').append($(option));
  });
}

var wgms_classes_success = function(data, textStatus, jqXHR) {
  $.each(data, function(index, value) {
    var option = new Option(value['primary_classification'] + ' -- ' + value['description'], value['primary_classification']);
    $('#wgms_class').append($(option));
  })
}

function call_populate_select_endpoint(endpoint, successFunc) {
  $.ajax({
    url: serviceURL + '/' + endpoint,
    success: successFunc,
    complete: function(jqXHR, textStatus) {
      console.log(jqXHR);
      console.log(textStatus);
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
}

function populate_selects() {
  call_populate_select_endpoint('countries', countries_success);
  call_populate_select_endpoint('rc_list', rc_list_success);
  call_populate_select_endpoint('wgms_classifications', wgms_classes_success);
}

var setup_results = function() {
  // table is global as it will be used for later searches.
  table = $('#results').DataTable({
    paging:false,
    fixedHeader: {header:true, footer:false},
    scrollY:400,
    searching:false,
    order:[],
    info:false,
    scrollCollapse:true
  });

  populate_selects();
}

setup_results();

$('#next-page').click( function (e) {
  e.preventDefault();
  var offset = parseInt($('#results_offset').val());
  var pageSize = parseInt($('#results_count').val());
  var total = parseInt($('#results_total').val());
  if (offset + pageSize <= total ) {
    $('#results_offset').val(offset + pageSize);
    $('#searchform').submit();
    $('.dataTables_wrapper').hide();
  }
})
$('#prev-page').click( function (e) {
  e.preventDefault();
  var offset = parseInt($('#results_offset').val());
  var pageSize = parseInt($('#results_count').val());
  var total = parseInt($('#results_total').val());
  if (offset - pageSize >= 0) {
    $('#results_offset').val(offset - pageSize);
    $('#searchform').submit();
    $('.dataTables_wrapper').hide();
  }
})


$('#search-toggle').click(function () {
  if ($('#search-toggle-img').attr('src') === '../images/triangle_closed.png') {
    openSearchSection();
  } else {
    closeSearchSection();
  }
});

$('#new-search').click(openSearchSection);
