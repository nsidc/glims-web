// Define background layers

// OpenStreetMap, they're default layer
var osm_standard = new ol.layer.Tile({
  title: 'Open Street Map (standard)',
  type: 'base',
  visible: true,
  source: new ol.source.OSM({
    url: "https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    attributions: [
      new ol.Attribution({
        html: 'Basemap &copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap contributors</a>'
      }),
    ]
  })
});

var thunderforest_apikey = '40ff49606f9047d99bd55ccd979b3256';
var osm_thunderforest_outdoors = new ol.layer.Tile({
  title: 'Open Street Map (local + English names)',
  type: 'base',
  visible: true,
  source: new ol.source.OSM({
    url: "https://{a-c}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png" + "?apikey=" + thunderforest_apikey,
    attributions: [
      new ol.Attribution({
        html: 'Basemap &copy; <a href="https://www.thunderforest.com" target="_blank">Thunderforest</a>'
      }),
      new ol.Attribution({
        html: 'Basemap data &copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap contributors</a>'
      })
    ]
  })
});

var osm_thunderforest_landscape = new ol.layer.Tile({
  title: 'Thunderforest Landscape',
  type: 'base',
  visible: true,
  source: new ol.source.OSM({
    url: "https://{a-c}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png" + "?apikey=" + thunderforest_apikey,
    attributions: [
      new ol.Attribution({
        html: 'Basemap &copy; <a href="https://www.thunderforest.com" target="_blank">Thunderforest</a>'
      }),
      new ol.Attribution({
        html: 'Basemap data &copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap contributors</a>'
      })
    ]
  })
});

var osm_thunderforest_transport = new ol.layer.Tile({
  title: 'Thunderforest Transport',
  type: 'base',
  visible: true,
  source: new ol.source.OSM({
    url: "https://{a-c}.tile.thunderforest.com/transport/{z}/{x}/{y}.png" + "?apikey=" + thunderforest_apikey,
    attributions: [
      new ol.Attribution({
        html: 'Basemap &copy; <a href="https://www.thunderforest.com" target="_blank">Thunderforest</a>'
      }),
      new ol.Attribution({
        html: 'Basemap data &copy; <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap contributors</a>'
      })
    ]
  })
});

// Mapbox layers

var mapbox_apikey = 'pk.eyJ1IjoiYnJ1Y2VyYXVwIiwiYSI6ImNqMmhyZDIxYjAwdXgzOWxnaGd4dndzanQifQ.jroxt_fSTHTj01e7Sc7jAw';
mapbox_satellite = new ol.layer.Tile({
  title: 'Mapbox Satellite Imagery',
  type: 'base',
  visible: 'true',
  source: new ol.source.XYZ({
    url: 'https://api.mapbox.com/styles/v1/mapbox/satellite-v9/tiles/256/{z}/{x}/{y}?access_token=' + mapbox_apikey,
    attributions: [
      new ol.Attribution({
        html: 'Basemap &copy; <a href="https://www.mapbox.com" target="_blank">Mapbox.com</a>'
      }),
    ]
  })
});


// NASA Global Image Browse Service (GIBS)

var gibs_base_url = "//map1{a-c}.vis.earthdata.nasa.gov/wmts-webmerc/";

// This one doesn't work in other than "geographic" projection
var gibs_aster_dem_src = new ol.source.XYZ({
  url: gibs_base_url +
    "ASTER_GDEM_Color_Shaded_Relief/default/2015-06-15/" +
    "GoogleMapsCompatible_Level9/{z}/{y}/{x}.jpg",
  attributions: [
    new ol.Attribution({
      html: 'Basemap from <a href="https://wiki.earthdata.nasa.gov/display/GIBS/Global+Imagery+Browse+Services+-+GIBS" target="_blank">NASA GIBS</a>'
    }),
  ]
});

var gibs_bluemarble_src = new ol.source.XYZ({
  url: gibs_base_url +
    "BlueMarble_ShadedRelief_Bathymetry/default/2013-06-15/" +
    "GoogleMapsCompatible_Level8/{z}/{y}/{x}.jpg",
  attributions: [
    new ol.Attribution({
      html: 'Basemap from <a href="https://wiki.earthdata.nasa.gov/display/GIBS/Global+Imagery+Browse+Services+-+GIBS" target="_blank">NASA GIBS</a>'
    }),
  ]
});

// This imagery is from a particular time, with clouds
var gibs_viirs_truecolor_src = new ol.source.XYZ({
  url: gibs_base_url +
    "VIIRS_SNPP_CorrectedReflectance_TrueColor/default/2016-06-15/" +
    "GoogleMapsCompatible_Level9/{z}/{y}/{x}.jpg",
  attributions: [
    new ol.Attribution({
      html: 'Basemap from <a href="https://wiki.earthdata.nasa.gov/display/GIBS/Global+Imagery+Browse+Services+-+GIBS" target="_blank">NASA GIBS</a>'
    }),
  ]
});

var gibs_bluemarble = new ol.layer.Tile({
  title: 'MODIS True Reflectance',
  type: 'base',
  visible: false,
  source: gibs_bluemarble_src
});

/*
var gibs_aster_dem = new ol.layer.Tile({
  title: 'ASTER Digital Elevation Model',
  type: 'base',
  visible: false,
  source: gibs_aster_dem_src
});
*/

var gibs_viirs_truecolor = new ol.layer.Tile({
  title: 'VIIRS SNPP Correct Reflectance True Color',
  type: 'base',
  visible: false,
  source: gibs_viirs_truecolor_src
});


// Combine background layers into group
var backgroundLayers = new ol.layer.Group({
  'title': 'Base maps',
  mytype: 'group',
  layers: [mapbox_satellite, osm_thunderforest_transport, osm_thunderforest_landscape, osm_thunderforest_outdoors]
});


//---- GeoServer Layers-----

// DCW data layer: migrated

var dcwSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "GLIMS:dcw_glaciers"},
  servertype: 'geoserver'
});

var dcwLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:dcw_glaciers"/>',
  source: dcwSource,
  visible: false
});

// FOG data layer: migrated

var fogSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "GLIMS:FOG_points"},
  attributions: [
    new ol.Attribution({
      html: 'FoG data from <a href="https://www.gtn-g.org/data_catalogue_fog.html" target="_blank">GTN-G and WGMS</a>'
    })
  ],
  servertype: 'geoserver'
});

var fogLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:FOG_points"/>',
  source: fogSource
});

// GlaThiDa data layer: migrated

var glathidaSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "GLIMS:glathida_points"},
  attributions: [
    new ol.Attribution({
      html: 'GlaThiDa data from <a href="https://www.gtn-g.org/data_catalogue_glathida.html" target="_blank">GTN-G and WGMS</a>'
    })
  ],
  servertype: 'geoserver'
});

var glathidaLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:glathida_points"/>',
  source: glathidaSource
});

// GLIMS data layer uses GeoWeb Cache endpoint: migrated

var geoWebCacheUrl = mapservURL.slice(0, -4) + '/gwc/service'
var glimsGlaciersSource = new ol.source.TileWMS({
  url: geoWebCacheUrl,
  params: {LAYERS: "GLIMS:GLIMS_GLACIERS", "TILED": true},
  attributions: [
    new ol.Attribution({
      html: 'GLIMS data from <a href="https://www.glims.org" target="_blank">GTN-G and GLIMS</a>'
    })
  ],
  servertype: 'geoserver'
});

var glimsGlaciersLayer = new ol.layer.Tile({
  title: 'GLIMS glacier outlines<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img align="middle" src="' +
    mapservURL + getlegend + '&layer=GLIMS_Glacier_Outlines"/>',
  source: glimsGlaciersSource
});

// GLIMS RC Outlines data layer: MIGRATED

var glimsRegionsSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "GLIMS:GLIMS_REGIONS"},
  servertype: 'geoserver'
});

var glimsRegionsLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:GLIMS_REGIONS"/>',
  source: glimsRegionsSource,
  visible: false
});

// GPC data layer: MIGRATED

var gpcSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "GLIMS:glacier_photos"},
  attributions: [
    new ol.Attribution({
      html: 'Glacier Photo data from <a href="https://www.gtn-g.org/data_catalogue_gpc.html" target="_blank">GTN-G and GLIMS</a>'
    })
  ],
  servertype: 'geoserver'
});

var gpcLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:glacier_photos"/>',
  source: gpcSource
});

// RGI6 data layer MIGRATED

var rgi6Source = new ol.source.TileWMS({
  url: geoWebCacheUrl,
  params: {LAYERS: "GLIMS:RGI", "TILED": true},
  attributions: [
    new ol.Attribution({
      html: 'RGI V6 data from <a href="https://www.glims.org/RGI" target="_blank">GLIMS</a>'
    })
  ],
  servertype: 'geoserver'
});

var rgi6Layer = new ol.layer.Tile({
  // This is a workaround, just requesting a legend for one of the grouped layers
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:RGI_WesternCanadaUS"/>',
  source: rgi6Source,
  visible: false,
});

// RGI7 data layer MIGRATED

var rgi7Source = new ol.source.TileWMS({
  url: geoWebCacheUrl,
  params: {LAYERS: "GLIMS:RGI70", "TILED": true},
  attributions: [
    new ol.Attribution({
      html: 'RGI V7.0 data from <a href="https://www.glims.org/RGI" target="_blank">GLIMS</a>'
    })
  ],
  servertype: 'geoserver'
});

var rgi7Layer = new ol.layer.Tile({
  // This is a workaround, just requesting a legend for one of the grouped layers
    title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:RGI2000-v7.0-G-02_western_canada_usa_epsg3857"/>',
  source: rgi7Source,
  visible: true,
});

// GTN-G Order 1 Regions data layer MIGRATED

var gtngO1Source = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "glims_gtng_o1regions"},
  servertype: 'geoserver'
});

var gtngO1Layer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:glims_gtng_o1regions"/>',
  source: gtngO1Source,
  visible: false
});

// GTN-G Order 2 Regions data layer MIGRATED

var gtngO2Source = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "glims_gtng_o2regions"},
  servertype: 'geoserver'
});

var gtngO2Layer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:glims_gtng_o2regions"/>',
  source: gtngO2Source,
  visible: false
});

// Extinct Glaciers layer

var extinct_gl_Source = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "GLIMS:extinct_glaciers_view"},
  attributions: [
    new ol.Attribution({
      html: 'Extinct glacier info provided by GLIMS community.'
    })
  ],
  servertype: 'geoserver'
});

var extinct_gl_Layer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:extinct_glaciers_view"/>',
  source: extinct_gl_Source,
  visible: false
});

// WGI data layer MIGRATED

var wgiSource = new ol.source.ImageWMS({
  url: mapservURL,
  params: {LAYERS: "WGI_points"},
  attributions: [
    new ol.Attribution({
      html: 'WGI data from <a href="https://www.gtn-g.org/data_catalogue_wgi.html" target="_blank">GTN-G and GLIMS</a>'
    })
  ],
  servertype: 'geoserver'
});

var wgiLayer = new ol.layer.Image({
  title: '<img src="' + mapservURL + getlegend + '&layer=GLIMS:WGI_points"/>',
  source: wgiSource
});

// Define layer groupings

var glims_layers = [gpcLayer, wgiLayer, dcwLayer, glimsRegionsLayer, extinct_gl_Layer,
                    gtngO2Layer, gtngO1Layer, rgi6Layer, rgi7Layer, glimsGlaciersLayer];
var gtng_layers = [gpcLayer, fogLayer, glathidaLayer, wgiLayer, rgi6Layer, rgi7Layer, glimsGlaciersLayer];

var gtngOverlays = new ol.layer.Group({
  title: 'Glacier Databases',
  mytype: 'group',
  layers: gtng ? gtng_layers : glims_layers
});

// Define the Map

var mapview = new ol.View({
  center: ol.proj.transform([-70, -3], 'EPSG:4326', 'EPSG:3857'),
  zoom: 5,
  minZoom: 1,
  maxZoom: 14
});

var map = new ol.Map({
  target: 'map',
  layers: [backgroundLayers, gtngOverlays],
  view: mapview
});

// Add map controls

var mousePositionControl = new ol.control.MousePosition({
  coordinateFormat: ol.coordinate.createStringXY(4),
  projection: 'EPSG:4326',
  undefinedHTML: '&nbsp;'
});
map.addControl(mousePositionControl);
map.addControl(new ol.control.LayerSwitcher());
map.addControl(new ol.control.ScaleLine());

var lowleft = ol.proj.transform([-180, -60], 'EPSG:4326', 'EPSG:3857');
var upright = ol.proj.transform([180, 60], 'EPSG:4326', 'EPSG:3857');
var extent_control = new ol.control.ZoomToExtent({
  extent: [lowleft[0], lowleft[1], upright[0], upright[1]]
});
map.addControl(extent_control);
