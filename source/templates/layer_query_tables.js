(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['layer_query_tables'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <form name=send_extent action="
    + alias4(((helper = (helper = lookupProperty(helpers,"getDataUrl") || (depth0 != null ? lookupProperty(depth0,"getDataUrl") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"getDataUrl","hash":{},"data":data,"loc":{"start":{"line":2,"column":32},"end":{"line":2,"column":46}}}) : helper)))
    + " target=\"_blank\" method=GET>\n    <table class='table table-condensed table-hover'>\n      <caption>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"gtng") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":5,"column":8},"end":{"line":9,"column":15}}})) != null ? stack1 : "")
    + "        <br/>\n        <input type=submit value='Download these GLIMS glacier outlines'>\n      </caption>\n      <tr>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Static' target=\"_blank\">Glacier Name</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Static' target=\"_blank\">Glacier ID</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Dynamic' target=\"_blank\">Analysis ID</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Dynamic' target=\"_blank\">Area, km<sup>2</sup></a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Image' target=\"_blank\">As-of Date</a>\n        </th>\n        <th> Source </th>\n        <th>More Info</th>\n      </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"glims_points") : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":32,"column":6},"end":{"line":44,"column":15}}})) != null ? stack1 : "")
    + "\n      <input type='hidden' name='download_type' value='id'>\n      <input type='hidden' name='clause' value='"
    + alias4(((helper = (helper = lookupProperty(helpers,"glacIdStr") || (depth0 != null ? lookupProperty(depth0,"glacIdStr") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glacIdStr","hash":{},"data":data,"loc":{"start":{"line":47,"column":48},"end":{"line":47,"column":61}}}) : helper)))
    + "'>\n\n    </table>\n  </form>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_glims.html\" target=\"_blank\" class=\"glims_info\">GLIMS Glacier Outlines</a>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.glims.org/\" target=\"_blank\" class=\"glims_info\">GLIMS Glacier Outlines</a>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"glac_name") || (depth0 != null ? lookupProperty(depth0,"glac_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glac_name","hash":{},"data":data,"loc":{"start":{"line":34,"column":59},"end":{"line":34,"column":72}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"glac_id") || (depth0 != null ? lookupProperty(depth0,"glac_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glac_id","hash":{},"data":data,"loc":{"start":{"line":35,"column":59},"end":{"line":35,"column":70}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"anlys_id") || (depth0 != null ? lookupProperty(depth0,"anlys_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"anlys_id","hash":{},"data":data,"loc":{"start":{"line":36,"column":59},"end":{"line":36,"column":71}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"db_area") || (depth0 != null ? lookupProperty(depth0,"db_area") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"db_area","hash":{},"data":data,"loc":{"start":{"line":37,"column":59},"end":{"line":37,"column":70}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"src_date") || (depth0 != null ? lookupProperty(depth0,"src_date") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"src_date","hash":{},"data":data,"loc":{"start":{"line":38,"column":59},"end":{"line":38,"column":71}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"chief_affl") || (depth0 != null ? lookupProperty(depth0,"chief_affl") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"chief_affl","hash":{},"data":data,"loc":{"start":{"line":39,"column":59},"end":{"line":39,"column":73}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">\n            <a href=\"info.html?anlys_id="
    + alias4(((helper = (helper = lookupProperty(helpers,"anlys_id") || (depth0 != null ? lookupProperty(depth0,"anlys_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"anlys_id","hash":{},"data":data,"loc":{"start":{"line":41,"column":40},"end":{"line":41,"column":52}}}) : helper)))
    + "\" target=\"_blank\">More...</a>\n          </div></td>\n        </tr>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <form name=send_extent action="
    + alias4(((helper = (helper = lookupProperty(helpers,"getDataUrl") || (depth0 != null ? lookupProperty(depth0,"getDataUrl") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"getDataUrl","hash":{},"data":data,"loc":{"start":{"line":54,"column":32},"end":{"line":54,"column":46}}}) : helper)))
    + " target=\"_blank\" method=GET>\n    <table class='table table-condensed table-hover'>\n      <caption>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"gtng") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":57,"column":8},"end":{"line":61,"column":15}}})) != null ? stack1 : "")
    + "        <br/>\n        <input type=submit value='Download these GLIMS Glacier Outlines'>\n      </caption>\n      <tr>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Static' target=\"_blank\">Glacier Name</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Static' target=\"_blank\">Glacier ID</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Dynamic' target=\"_blank\">Analysis ID</a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_Dynamic' target=\"_blank\">Area, km<sup>2</sup></a>\n        </th>\n        <th>\n          <a href='http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Image' target=\"_blank\">As-of Date</a>\n        </th>\n        <th> Source </th>\n        <th>More Info</th>\n      </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"glac_lines") : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":84,"column":6},"end":{"line":96,"column":15}}})) != null ? stack1 : "")
    + "\n      <input type='hidden' name='download_type' value='id'>\n      <input type='hidden' name='clause' value='"
    + alias4(((helper = (helper = lookupProperty(helpers,"glacIdStr") || (depth0 != null ? lookupProperty(depth0,"glacIdStr") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glacIdStr","hash":{},"data":data,"loc":{"start":{"line":99,"column":48},"end":{"line":99,"column":61}}}) : helper)))
    + "'>\n\n    </table>\n  </form>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"gtng") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.program(13, data, 0),"data":data,"loc":{"start":{"line":108,"column":8},"end":{"line":112,"column":15}}})) != null ? stack1 : "")
    + "    </caption>\n    <tr>\n      <th>Glacier Name</th>\n      <th>RGI 6 Glacier ID</th>\n      <th>Begin date</th>\n      <th>End date</th>\n      <th>Total Area, km<sup>2</sup></th>\n      <th>Minimum Elevation, m</th>\n      <th>Median Elevation, m</th>\n      <th>Maximum Elevation, m</th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"rgi6") : depth0),{"name":"each","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":124,"column":4},"end":{"line":135,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_rgi.html\" target=\"_blank\" class='rgi_info'>Randolph 6 Glacier Inventory</a>\n";
},"13":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.glims.org/RGI/\" target=\"_blank\" class='rgi_info'>Randolph 6 Glacier Inventory</a>\n";
},"15":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"NAME") || (depth0 != null ? lookupProperty(depth0,"NAME") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NAME","hash":{},"data":data,"loc":{"start":{"line":126,"column":57},"end":{"line":126,"column":65}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"RGIID") || (depth0 != null ? lookupProperty(depth0,"RGIID") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RGIID","hash":{},"data":data,"loc":{"start":{"line":127,"column":57},"end":{"line":127,"column":66}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"BGNDATE") || (depth0 != null ? lookupProperty(depth0,"BGNDATE") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"BGNDATE","hash":{},"data":data,"loc":{"start":{"line":128,"column":57},"end":{"line":128,"column":68}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"ENDDATE") || (depth0 != null ? lookupProperty(depth0,"ENDDATE") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ENDDATE","hash":{},"data":data,"loc":{"start":{"line":129,"column":57},"end":{"line":129,"column":68}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"AREA") || (depth0 != null ? lookupProperty(depth0,"AREA") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"AREA","hash":{},"data":data,"loc":{"start":{"line":130,"column":57},"end":{"line":130,"column":65}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"ZMIN") || (depth0 != null ? lookupProperty(depth0,"ZMIN") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ZMIN","hash":{},"data":data,"loc":{"start":{"line":131,"column":57},"end":{"line":131,"column":65}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"ZMED") || (depth0 != null ? lookupProperty(depth0,"ZMED") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ZMED","hash":{},"data":data,"loc":{"start":{"line":132,"column":57},"end":{"line":132,"column":65}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"ZMAX") || (depth0 != null ? lookupProperty(depth0,"ZMAX") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ZMAX","hash":{},"data":data,"loc":{"start":{"line":133,"column":57},"end":{"line":133,"column":65}}}) : helper)))
    + "</div></td>\n      </tr>\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"gtng") : depth0),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.program(20, data, 0),"data":data,"loc":{"start":{"line":142,"column":8},"end":{"line":146,"column":15}}})) != null ? stack1 : "")
    + "    </caption>\n    <tr>\n      <th>Glacier Name</th>\n      <th>RGI 7.0 Glacier ID</th>\n      <th>Source Date</th>\n      <th>Total Area, km<sup>2</sup></th>\n      <th>Minimum Elevation, m</th>\n      <th>Median Elevation, m</th>\n      <th>Mean Elevation, m</th>\n      <th>Maximum Elevation, m</th>\n      <th>GLIMS Analysis ID</th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"rgi7") : depth0),{"name":"each","hash":{},"fn":container.program(22, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":159,"column":4},"end":{"line":171,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"18":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_rgi.html\" target=\"_blank\" class='rgi_info_new'>Randolph 7.0 Glacier Inventory</a>\n";
},"20":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.glims.org/RGI/\" target=\"_blank\" class='rgi_info_new'>Randolph 7.0 Glacier Inventory</a>\n";
},"22":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"glac_name") || (depth0 != null ? lookupProperty(depth0,"glac_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glac_name","hash":{},"data":data,"loc":{"start":{"line":161,"column":57},"end":{"line":161,"column":70}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"rgi_id") || (depth0 != null ? lookupProperty(depth0,"rgi_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"rgi_id","hash":{},"data":data,"loc":{"start":{"line":162,"column":57},"end":{"line":162,"column":67}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"src_date") || (depth0 != null ? lookupProperty(depth0,"src_date") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"src_date","hash":{},"data":data,"loc":{"start":{"line":163,"column":57},"end":{"line":163,"column":69}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"area_km2") || (depth0 != null ? lookupProperty(depth0,"area_km2") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"area_km2","hash":{},"data":data,"loc":{"start":{"line":164,"column":57},"end":{"line":164,"column":69}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"zmin_m") || (depth0 != null ? lookupProperty(depth0,"zmin_m") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"zmin_m","hash":{},"data":data,"loc":{"start":{"line":165,"column":57},"end":{"line":165,"column":67}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"zmed_m") || (depth0 != null ? lookupProperty(depth0,"zmed_m") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"zmed_m","hash":{},"data":data,"loc":{"start":{"line":166,"column":57},"end":{"line":166,"column":67}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"zmean_m") || (depth0 != null ? lookupProperty(depth0,"zmean_m") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"zmean_m","hash":{},"data":data,"loc":{"start":{"line":167,"column":57},"end":{"line":167,"column":68}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"zmax_m") || (depth0 != null ? lookupProperty(depth0,"zmax_m") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"zmax_m","hash":{},"data":data,"loc":{"start":{"line":168,"column":57},"end":{"line":168,"column":67}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"anlys_id") || (depth0 != null ? lookupProperty(depth0,"anlys_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"anlys_id","hash":{},"data":data,"loc":{"start":{"line":169,"column":57},"end":{"line":169,"column":69}}}) : helper)))
    + "</div></td>\n      </tr>\n";
},"24":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n      <a href=\"http://glims.colorado.edu/php_utils/nsidc_rc_table_public.php\" target=\"_blank\" class='glims_regions'>GLIMS Regions</a>\n    </caption>\n    <tr>\n      <th>RC ID</th>\n      <th>Affiliation</th>\n      <th>Geographic Location</th>\n      <th>Given Names</th>\n      <th>Surname</th>\n      <th>Primary URL</th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"RC_Outlines") : depth0),{"name":"each","hash":{},"fn":container.program(25, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":188,"column":4},"end":{"line":197,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"25":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"rc_id") || (depth0 != null ? lookupProperty(depth0,"rc_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"rc_id","hash":{},"data":data,"loc":{"start":{"line":190,"column":57},"end":{"line":190,"column":66}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"affiliation") || (depth0 != null ? lookupProperty(depth0,"affiliation") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"affiliation","hash":{},"data":data,"loc":{"start":{"line":191,"column":57},"end":{"line":191,"column":72}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"geog_area") || (depth0 != null ? lookupProperty(depth0,"geog_area") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"geog_area","hash":{},"data":data,"loc":{"start":{"line":192,"column":57},"end":{"line":192,"column":70}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"givennames") || (depth0 != null ? lookupProperty(depth0,"givennames") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"givennames","hash":{},"data":data,"loc":{"start":{"line":193,"column":57},"end":{"line":193,"column":71}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"surname") || (depth0 != null ? lookupProperty(depth0,"surname") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"surname","hash":{},"data":data,"loc":{"start":{"line":194,"column":57},"end":{"line":194,"column":68}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\"><a href=\""
    + alias4(((helper = (helper = lookupProperty(helpers,"rc_url_prime") || (depth0 != null ? lookupProperty(depth0,"rc_url_prime") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"rc_url_prime","hash":{},"data":data,"loc":{"start":{"line":195,"column":66},"end":{"line":195,"column":82}}}) : helper)))
    + "\" target=\"_blank\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"rc_url_prime") || (depth0 != null ? lookupProperty(depth0,"rc_url_prime") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"rc_url_prime","hash":{},"data":data,"loc":{"start":{"line":195,"column":100},"end":{"line":195,"column":116}}}) : helper)))
    + "</a></div></td>\n      </tr>\n";
},"27":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"gtng") : depth0),{"name":"if","hash":{},"fn":container.program(28, data, 0),"inverse":container.program(30, data, 0),"data":data,"loc":{"start":{"line":204,"column":8},"end":{"line":208,"column":15}}})) != null ? stack1 : "")
    + "    </caption>\n    <tr>\n      <th>Political Unit</th>\n      <th>Glacier Name</th>\n      <th>WGI Glacier ID</th>\n      <th>Total Area, km<sup>2</sup></th>\n      <th>Mean Length</th>\n      <th>Min. Elevation, m</th>\n      <th>Mean Elevation, m</th>\n      <th>Max. Elevation, m</th>\n      <th>Photo Year</th>\n      <th>Topo Year</th>\n      <th>Data Contributor</th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"WGI_points") : depth0),{"name":"each","hash":{},"fn":container.program(32, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":223,"column":4},"end":{"line":237,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"28":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_wgi.html\" class='wgi_info' target='_blank'>World Glacier Inventory</a>\n";
},"30":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://nsidc.org/data/glacier_inventory/\" target='_blank' class='wgi_info'>World Glacier Inventory</a>\n";
},"32":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"political_unit") || (depth0 != null ? lookupProperty(depth0,"political_unit") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"political_unit","hash":{},"data":data,"loc":{"start":{"line":225,"column":57},"end":{"line":225,"column":75}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"glacier_name") || (depth0 != null ? lookupProperty(depth0,"glacier_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glacier_name","hash":{},"data":data,"loc":{"start":{"line":226,"column":57},"end":{"line":226,"column":73}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"wgi_glacier_id") || (depth0 != null ? lookupProperty(depth0,"wgi_glacier_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"wgi_glacier_id","hash":{},"data":data,"loc":{"start":{"line":227,"column":57},"end":{"line":227,"column":75}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"total_area") || (depth0 != null ? lookupProperty(depth0,"total_area") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_area","hash":{},"data":data,"loc":{"start":{"line":228,"column":57},"end":{"line":228,"column":71}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"mean_length") || (depth0 != null ? lookupProperty(depth0,"mean_length") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"mean_length","hash":{},"data":data,"loc":{"start":{"line":229,"column":57},"end":{"line":229,"column":72}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"min_elev") || (depth0 != null ? lookupProperty(depth0,"min_elev") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"min_elev","hash":{},"data":data,"loc":{"start":{"line":230,"column":57},"end":{"line":230,"column":69}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"mean_elev") || (depth0 != null ? lookupProperty(depth0,"mean_elev") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"mean_elev","hash":{},"data":data,"loc":{"start":{"line":231,"column":57},"end":{"line":231,"column":70}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"max_elev") || (depth0 != null ? lookupProperty(depth0,"max_elev") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"max_elev","hash":{},"data":data,"loc":{"start":{"line":232,"column":57},"end":{"line":232,"column":69}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"photo_year") || (depth0 != null ? lookupProperty(depth0,"photo_year") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photo_year","hash":{},"data":data,"loc":{"start":{"line":233,"column":57},"end":{"line":233,"column":71}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"topo_year") || (depth0 != null ? lookupProperty(depth0,"topo_year") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"topo_year","hash":{},"data":data,"loc":{"start":{"line":234,"column":57},"end":{"line":234,"column":70}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"data_contributor") || (depth0 != null ? lookupProperty(depth0,"data_contributor") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"data_contributor","hash":{},"data":data,"loc":{"start":{"line":235,"column":57},"end":{"line":235,"column":77}}}) : helper)))
    + "</div></td>\n      </tr>\n";
},"34":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n      <caption>\n        <a href=\"http://www.gtn-g.org/data_catalogue_glathida.html\"\n           target=\"_blank\" class=\"glathida_info\">Glacier Thickness Dataset</a>\n      </caption>\n    <tr>\n      <th>Political Unit</th>\n      <th>Glacier Name</th>\n      <th>Record ID</th>\n      <th>Mean Thickness</th>\n      <th>Max Thickness</th>\n      <th>Method</th>\n      <th>Survey Date</th>\n      <th>Principal Investigator</th>\n      <th>Agency</th>\n      <th>References</th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"glathida_points") : depth0),{"name":"each","hash":{},"fn":container.program(35, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":259,"column":4},"end":{"line":272,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"35":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"PU") || (depth0 != null ? lookupProperty(depth0,"PU") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"PU","hash":{},"data":data,"loc":{"start":{"line":261,"column":57},"end":{"line":261,"column":63}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"Name") || (depth0 != null ? lookupProperty(depth0,"Name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Name","hash":{},"data":data,"loc":{"start":{"line":262,"column":57},"end":{"line":262,"column":65}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"RecordID") || (depth0 != null ? lookupProperty(depth0,"RecordID") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RecordID","hash":{},"data":data,"loc":{"start":{"line":263,"column":57},"end":{"line":263,"column":69}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"MeanDepth") || (depth0 != null ? lookupProperty(depth0,"MeanDepth") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"MeanDepth","hash":{},"data":data,"loc":{"start":{"line":264,"column":57},"end":{"line":264,"column":70}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"MaxDepth") || (depth0 != null ? lookupProperty(depth0,"MaxDepth") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"MaxDepth","hash":{},"data":data,"loc":{"start":{"line":265,"column":57},"end":{"line":265,"column":69}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"Method") || (depth0 != null ? lookupProperty(depth0,"Method") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Method","hash":{},"data":data,"loc":{"start":{"line":266,"column":57},"end":{"line":266,"column":67}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"SurveyDate") || (depth0 != null ? lookupProperty(depth0,"SurveyDate") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"SurveyDate","hash":{},"data":data,"loc":{"start":{"line":267,"column":57},"end":{"line":267,"column":71}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"PrincInv") || (depth0 != null ? lookupProperty(depth0,"PrincInv") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"PrincInv","hash":{},"data":data,"loc":{"start":{"line":268,"column":57},"end":{"line":268,"column":69}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"Agency") || (depth0 != null ? lookupProperty(depth0,"Agency") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"Agency","hash":{},"data":data,"loc":{"start":{"line":269,"column":57},"end":{"line":269,"column":67}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"References") || (depth0 != null ? lookupProperty(depth0,"References") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"References","hash":{},"data":data,"loc":{"start":{"line":270,"column":57},"end":{"line":270,"column":71}}}) : helper)))
    + "</div></td>\n      </tr>\n";
},"37":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n      <a href=\"http://www.gtn-g.org/data_catalogue_fog.html\" target=\"_blank\" class=\"fog_info\">Fluctuations of Glaciers</a>\n    </caption>\n    <tr>\n      <th>Political Unit</th>\n      <th>Glacier Name</th>\n      <th>WGMS ID</th>\n      <th>Measurement type</th>\n      <th>Num Observation</th>\n      <th>1st Ref. Year</th>\n      <th>1st Survey Year</th>\n      <th>Last Survey Year</th>\n      <th>Current Status</th>\n      <th>Principal Investigator</th>\n      <th>See graph</th>\n      <th>Get minimal data series</th>\n      <th>Order full data series</th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"FOG_points") : depth0),{"name":"each","hash":{},"fn":container.program(38, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":296,"column":4},"end":{"line":312,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"38":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"PU") || (depth0 != null ? lookupProperty(depth0,"PU") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"PU","hash":{},"data":data,"loc":{"start":{"line":298,"column":59},"end":{"line":298,"column":65}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"NAME") || (depth0 != null ? lookupProperty(depth0,"NAME") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NAME","hash":{},"data":data,"loc":{"start":{"line":299,"column":59},"end":{"line":299,"column":67}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"WGMS_ID") || (depth0 != null ? lookupProperty(depth0,"WGMS_ID") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"WGMS_ID","hash":{},"data":data,"loc":{"start":{"line":300,"column":59},"end":{"line":300,"column":70}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"source_des") || (depth0 != null ? lookupProperty(depth0,"source_des") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"source_des","hash":{},"data":data,"loc":{"start":{"line":301,"column":59},"end":{"line":301,"column":73}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"NoObs") || (depth0 != null ? lookupProperty(depth0,"NoObs") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"NoObs","hash":{},"data":data,"loc":{"start":{"line":302,"column":59},"end":{"line":302,"column":68}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"firstRY") || (depth0 != null ? lookupProperty(depth0,"firstRY") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"firstRY","hash":{},"data":data,"loc":{"start":{"line":303,"column":59},"end":{"line":303,"column":70}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"firstSY") || (depth0 != null ? lookupProperty(depth0,"firstSY") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"firstSY","hash":{},"data":data,"loc":{"start":{"line":304,"column":59},"end":{"line":304,"column":70}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"lastSY") || (depth0 != null ? lookupProperty(depth0,"lastSY") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"lastSY","hash":{},"data":data,"loc":{"start":{"line":305,"column":59},"end":{"line":305,"column":69}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"CurrentSta") || (depth0 != null ? lookupProperty(depth0,"CurrentSta") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"CurrentSta","hash":{},"data":data,"loc":{"start":{"line":306,"column":59},"end":{"line":306,"column":73}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"LatestPI") || (depth0 != null ? lookupProperty(depth0,"LatestPI") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"LatestPI","hash":{},"data":data,"loc":{"start":{"line":307,"column":59},"end":{"line":307,"column":71}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\"><a href='"
    + alias4(((helper = (helper = lookupProperty(helpers,"GraphLink") || (depth0 != null ? lookupProperty(depth0,"GraphLink") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"GraphLink","hash":{},"data":data,"loc":{"start":{"line":308,"column":68},"end":{"line":308,"column":81}}}) : helper)))
    + "' target='_blank'>See graph</a></div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\"><a href='"
    + alias4(((helper = (helper = lookupProperty(helpers,"DataLink") || (depth0 != null ? lookupProperty(depth0,"DataLink") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"DataLink","hash":{},"data":data,"loc":{"start":{"line":309,"column":68},"end":{"line":309,"column":80}}}) : helper)))
    + "'>Get data</a></div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\"><a href='"
    + alias4(((helper = (helper = lookupProperty(helpers,"MailToLink") || (depth0 != null ? lookupProperty(depth0,"MailToLink") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"MailToLink","hash":{},"data":data,"loc":{"start":{"line":310,"column":68},"end":{"line":310,"column":82}}}) : helper)))
    + "'>Order full data</a></div></td>\n        </tr>\n";
},"40":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"gtng") : depth0),{"name":"if","hash":{},"fn":container.program(41, data, 0),"inverse":container.program(43, data, 0),"data":data,"loc":{"start":{"line":319,"column":8},"end":{"line":323,"column":15}}})) != null ? stack1 : "")
    + "    </caption>\n    <tr>\n      <th>Country</th>\n      <th>Glacier Name</th>\n      <th>Photo ID</th>\n      <th>Photo Date</th>\n      <th>Photographer Name</th>\n      <th>Source</th>\n      <th>More Info</th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"glacier_photos") : depth0),{"name":"each","hash":{},"fn":container.program(45, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":334,"column":4},"end":{"line":346,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"41":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://www.gtn-g.org/data_catalogue_gpc.html\" target=\"_blank\" class=\"gpc_info\">Glacier Photograph Collection</a>\n";
},"43":function(container,depth0,helpers,partials,data) {
    return "          <a href=\"http://nsidc.org/data/glacier_photo/\" target=\"_blank\" class=\"gpc_info\">Glacier Photograph Collection</a>\n";
},"45":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"country") || (depth0 != null ? lookupProperty(depth0,"country") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"country","hash":{},"data":data,"loc":{"start":{"line":336,"column":57},"end":{"line":336,"column":68}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"glacier_name") || (depth0 != null ? lookupProperty(depth0,"glacier_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glacier_name","hash":{},"data":data,"loc":{"start":{"line":337,"column":57},"end":{"line":337,"column":73}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"digital_file_id") || (depth0 != null ? lookupProperty(depth0,"digital_file_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"digital_file_id","hash":{},"data":data,"loc":{"start":{"line":338,"column":57},"end":{"line":338,"column":76}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"photo_date") || (depth0 != null ? lookupProperty(depth0,"photo_date") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photo_date","hash":{},"data":data,"loc":{"start":{"line":339,"column":57},"end":{"line":339,"column":71}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"photographer_name") || (depth0 != null ? lookupProperty(depth0,"photographer_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"photographer_name","hash":{},"data":data,"loc":{"start":{"line":340,"column":57},"end":{"line":340,"column":78}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"source") || (depth0 != null ? lookupProperty(depth0,"source") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"source","hash":{},"data":data,"loc":{"start":{"line":341,"column":57},"end":{"line":341,"column":67}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">\n          <a href=\"http://nsidc.org/data/glacier_photo/search/image_info/"
    + alias4(((helper = (helper = lookupProperty(helpers,"digital_file_id") || (depth0 != null ? lookupProperty(depth0,"digital_file_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"digital_file_id","hash":{},"data":data,"loc":{"start":{"line":343,"column":73},"end":{"line":343,"column":92}}}) : helper)))
    + "\" target=\"_blank\">More...</a>\n        </div></td>\n      </tr>\n";
},"47":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n    <caption>\n          Extinct Glaciers\n    </caption>\n    <tr>\n      <th>Glacier Name</th>\n      <th>GLIMS Glacier ID</th>\n      <th>Estimated Date of Disappearance</th>\n      <th>Margin of error in date (years)</th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"extinct_glaciers_view") : depth0),{"name":"each","hash":{},"fn":container.program(48, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":361,"column":4},"end":{"line":368,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"48":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "      <tr>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"glac_name") || (depth0 != null ? lookupProperty(depth0,"glac_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glac_name","hash":{},"data":data,"loc":{"start":{"line":363,"column":57},"end":{"line":363,"column":70}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"glac_id") || (depth0 != null ? lookupProperty(depth0,"glac_id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"glac_id","hash":{},"data":data,"loc":{"start":{"line":364,"column":57},"end":{"line":364,"column":68}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"gone_date") || (depth0 != null ? lookupProperty(depth0,"gone_date") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"gone_date","hash":{},"data":data,"loc":{"start":{"line":365,"column":57},"end":{"line":365,"column":70}}}) : helper)))
    + "</div></td>\n        <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"gone_dt_e") || (depth0 != null ? lookupProperty(depth0,"gone_dt_e") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"gone_dt_e","hash":{},"data":data,"loc":{"start":{"line":366,"column":57},"end":{"line":366,"column":70}}}) : helper)))
    + "</div></td>\n      </tr>\n";
},"50":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "  <table class='table table-condensed table-hover'>\n    <tr>\n      <td><div style=\"max-height:80px; overflow:auto;\">\n        <h2>\n          Countries\n        </h2>\n      </div></td>\n    </tr>\n    <tr>\n      <th>Country Name</th>\n      <th>Country Code</th>\n      <th>Population</th>\n      <th>Area, km<sup>2</sup></th>\n    </tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"Countries") : depth0),{"name":"each","hash":{},"fn":container.program(51, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":387,"column":4},"end":{"line":394,"column":13}}})) != null ? stack1 : "")
    + "  </table>\n";
},"51":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <tr>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"cntry_name") || (depth0 != null ? lookupProperty(depth0,"cntry_name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cntry_name","hash":{},"data":data,"loc":{"start":{"line":389,"column":59},"end":{"line":389,"column":73}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"country_code2") || (depth0 != null ? lookupProperty(depth0,"country_code2") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"country_code2","hash":{},"data":data,"loc":{"start":{"line":390,"column":59},"end":{"line":390,"column":76}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"pop_cntry") || (depth0 != null ? lookupProperty(depth0,"pop_cntry") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"pop_cntry","hash":{},"data":data,"loc":{"start":{"line":391,"column":59},"end":{"line":391,"column":72}}}) : helper)))
    + "</div></td>\n          <td><div style=\"max-height:80px; overflow:auto;\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"sqkm_cntry") || (depth0 != null ? lookupProperty(depth0,"sqkm_cntry") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"sqkm_cntry","hash":{},"data":data,"loc":{"start":{"line":392,"column":59},"end":{"line":392,"column":73}}}) : helper)))
    + "</div></td>\n        </tr>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"glims_points") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":51,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"glac_lines") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":53,"column":0},"end":{"line":103,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"rgi6") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":105,"column":0},"end":{"line":137,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"rgi7") : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":139,"column":0},"end":{"line":173,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"RC_Outlines") : depth0),{"name":"if","hash":{},"fn":container.program(24, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":175,"column":0},"end":{"line":199,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"WGI_points") : depth0),{"name":"if","hash":{},"fn":container.program(27, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":201,"column":0},"end":{"line":239,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"glathida_points") : depth0),{"name":"if","hash":{},"fn":container.program(34, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":241,"column":0},"end":{"line":274,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"FOG_points") : depth0),{"name":"if","hash":{},"fn":container.program(37, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":276,"column":0},"end":{"line":314,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"glacier_photos") : depth0),{"name":"if","hash":{},"fn":container.program(40, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":316,"column":0},"end":{"line":348,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"extinct_glaciers_view") : depth0),{"name":"if","hash":{},"fn":container.program(47, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":350,"column":0},"end":{"line":370,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"Countries") : depth0),{"name":"if","hash":{},"fn":container.program(50, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":372,"column":0},"end":{"line":396,"column":7}}})) != null ? stack1 : "");
},"useData":true});
})();