<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<link rel="STYLESHEET" href="../css/nsidc.css" type="text/css" media="SCREEN">
</head>
<body bgcolor="#FFFFFF">
<?php 
  include '../css/nsidc_header.php';
  include 'connect.php';
  $current_date = date("Y-m-d h:i:s");
  $glac_id= $_GET['glac_id'];  
  $sql="select glac_name, box2d(glacier_polys), line_type, release_okay_date from glacier_query_full3 WHERE line_type = 'glac_bound' AND glac_id = '$glac_id'"; 
  $result=pg_exec($dbh,$sql);
  $data=pg_fetch_array($result);

  print("<center><b>GLACIER NAME: $data[0]</b><br>");
  
  list($xy_min, $xy_max) = split(",",$data[1]);
  list($box, $min_values) = split("\(",$xy_min);
  list($minx, $miny) = split(" ",$min_values);
  list($maxx, $maxy) = split(" ",$xy_max);

  # Create a new SLD file that filters the glacier layer based on the IDs returned in the search
  #1 create the new temp SLD File
  $temp_dir = "/data/tmp/";
  $pid      = getmypid();
  $SLD_file = "SLD_" . $pid . ".xml";
  $file = $temp_dir . $SLD_file;

  # Open the file for writing
  $handle = fopen("$file", "w+");

  # Write the first line of the SLD File
  fwrite($handle, "<StyledLayerDescriptor version=\"1.0.0\">\n");

  $glac_layers = array("GLIMS_Segments","GLIMS_Points","GLIMS_Glacier_Polygons");
  $geom_layers = array("segments","points","glacier_polys");
  $colors      = array("#0002B0","#000080","#ff0000");

  # 2 Loop through the MapServer layers and write an SLD Layer filter for each one
  for ($j=0; $j < 3; $j++){
    fwrite($handle,
      "<NamedLayer>
        <Name>$glac_layers[$j]</Name>
        <UserStyle>
         <Title>xxx</Title>
          <FeatureTypeStyle>\r");

      if($j == 1){
        fwrite($handle,
         "<Rule>
           <Filter>
            <PropertyIsEqualTo>
             <PropertyName>glac_id</PropertyName>
             <Literal>$glac_id</Literal>
            </PropertyIsEqualTo>
           </Filter>
           <PointSymbolizer>
            <Geometry>
             <ogc:PropertyName>$geom_layers[$j]</ogc:PropertyName>
            </Geometry>
            <Graphic>
              <Mark>
               <WellKnownName>star</WellKnownName>
               <Fill>
                <CssParameter name=\"fill\">#0000ff</CssParameter>
               </Fill>
             </Mark>
             <Size>10.0</Size>
            </Graphic>
           </PointSymbolizer>
           <LineSymbolizer>
            <Geometry>
             <PropertyName>$geom_layers[$j]</PropertyName>
            </Geometry>
            <Stroke>
             <CssParameter name=\"stroke\">$colors[$j]</CssParameter>
            </Stroke>
           </LineSymbolizer>
          </Rule>\n");
      } else {
        fwrite($handle,
         "<Rule>
           <Filter>
            <PropertyIsEqualTo>
             <PropertyName>glac_id</PropertyName>
             <Literal>$glac_id</Literal>
            </PropertyIsEqualTo>
           </Filter>
           <LineSymbolizer>
            <Geometry>
             <PropertyName>$geom_layers[$j]</PropertyName>
            </Geometry>
            <Stroke>
             <CssParameter name=\"stroke\">$colors[$j]</CssParameter>
             <CssParameter name=\"stroke-width\">2.0</CssParameter>
            </Stroke>
           </LineSymbolizer>
          </Rule>\n");
    }
     fwrite($handle,
      "</FeatureTypeStyle>
        </UserStyle>
        </NamedLayer>\r");
  }

  # Write the last line of the SLD
  fwrite($handle, "</StyledLayerDescriptor>\n");

  
  $getmap="http://glims.colorado.edu/cgi-bin/glims_ogc?service=WMS&request=GetMap&version=1.1.1&layers=Source_Image_Rasters,GLIMS_GLACIERS&styles=&srs=EPSG:4326&BBOX=$minx,$miny,$maxx,$maxy&width=600&height=400&format=image/png&SLD=http://glims.colorado.edu/tmp/$SLD_file";
  echo "<img src='$getmap' border='0'></center><hr>";
  if ($data[2] <= $current_date){
    echo "<form name='download' method=POST action='../php_utils/get_data.php'>
           <input type='hidden' name='glac_id_list' value='$glac_id'>
           <center><input type=submit value='Download Glacier'></center>
          </form>";
  } else {
    
  }
  

?>
</body>
</html>
