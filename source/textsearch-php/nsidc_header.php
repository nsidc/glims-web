<?php
echo "<!-- vvvvvvvvvv SSI BELOW vvvvvvvvvv -->

<div id='navigation'>
<table width='800' border='0' cellpadding='0' cellspacing='0'>

  <tr>
    <td width='85' valign='bottom'><a href='http://nsidc.org/index.html'><img src='http://nsidc.org/ssi/images/nsidc_logo_top.gif' alt='NSIDC Logo' width='85' height='55' border='0' /></a></td>
    <td width='498' valign='bottom'><a href='http://nsidc.org/index.html'><img src='http://nsidc.org/ssi/images/nsidc_text.gif' alt='National Snow and Ice Data Center' width='294' height='26' border='0' class='style1' /></a></td>
    <td width='180' valign='middle' nowrap='nowrap' background='http://nsidc.org/ssi/images/snow_1.jpg' bgcolor='#FFFFFF'><form method='get' action='http://www.google.com/custom'>
        <div align='center'>
          <input name='q' type='text' class='SmallTextGray' value='Google Site Search' size='18' maxlength='255' onFocus='clearText(this)'/>
          <input name='sa' type='submit' value='Go' />
          <input type='hidden' name='cof' value='S:http://nsidc.org;AH:left;LH:60;L:http://nsidc.org/images/logo_nsidc_76x60.jpg;LW:76;AWFID:aad42effea8df8f4;' />

          <input type='hidden' name='domains' value='nsidc.org' />
          <input type='hidden' name='sitesearch' value='nsidc.org' checked='checked' />
        </div>
      </form></td>
  </tr>
  <tr>
    <td width='85' valign='top'><a href='http://nsidc.org/index.html'><img src='http://nsidc.org/ssi/images/nsidc_logo_bottom.gif' alt='NSIDC Logo' width='85' height='20' border='0' /></a></td>
    <td valign='top'><div>
        <ul id='p7menubar' onMouseover='P7_ExpMenu()'>

          <li><a class='trigger'>Data</a>
            <ul>
              <li><a href='http://nsidc.org/data/index.html'>Advanced Data Search</a></li>
              <li><a href='http://nsidc.org/data/methods.html'>More Data Discovery &amp; Access</a></li>
              <li><a href='http://nsidc.org/data/easytouse.html'>Easy-to-use Data Products</a></li>

                          <li><a href='http://nsidc.org/data/collections.html'>Data Collections</a></li>

              <li><a href='http://nsidc.org/data/tools/'>Data Software &amp; Programming Tools</a></li>
              <li><a href='http://nsidc.org/data/submit.html'>Data Submission</a></li>
                          <li><a href='http://nsidc.org/data/news.html'>Data News</a></li>
                          
            </ul>

          </li>
          <li><a class='trigger'>Centers &amp; Programs </a>

                <ul>
              <li><a href='http://nsidc.org/agdc/'>Antarctic Glaciological Data Center</a></li>
              <li><a href='http://nsidc.org/arcss/'>Arctic System Science Data Coordination Center</a>
                </li>

              
              <li><a href='http://nsidc.org/fgdc/'>IARC Frozen Ground Data Center</a>
                </li>
                                <li><a href='http://nsidc.org/ipydis/'>International Polar Year Data & Information Service (IPYDIS)</a></li>

              <li><a href='http://nsidc.org/noaa/'>NOAA at NSIDC
               </a></li>
              <li><a href='http://nsidc.org/daac/'>NSIDC Distributed Active Archive Center (NSIDC DAAC)</a> 
                </li>

              <li><a href='http://nsidc.org/usadcc'>U.S. Antarctic Data Coordination Center</a>
                </li>
              <li><a href='http://nsidc.org/wdc'>World Data Center for Glaciology, Boulder<br>
                </a></li>

              
            </ul>
          </li>
          <li><a class='trigger'>Science</a>

            <ul>
              <li><a href='http://nsidc.org/research/'>Scientists</a></li>
              <li><a href='http://nsidc.org/research/projects.html'>Research Projects</a></li>
              <!-- <li><a href='/research/proposed.html'>Proposed Research</a></li> -->

            </ul>
          </li>
          <li><a class='trigger'>About NSIDC</a>

            <ul>
              <li><a href='http://nsidc.org/about/expertise/overview.html'>Expertise</a></li>
              <li><a href='http://nsidc.org/about/services/data_docs.html'>Data &amp; Services</a></li>

              <li><a href='http://nsidc.org/about/sponsors.html'>NSIDC Sponsors</a></li>
              <li><a href='http://nsidc.org/about/contacts/directions.html'>Contacts</a></li>

              <li><a href='http://nsidc.org/about/jobs.html'>Jobs</a></li>
              <li><a href='http://nsidc.org/about/related_links.html'>Related Organizations</a></li>
            </ul>
          </li>
          <li><a class='trigger'>Publications</a>

            <ul>
              <li><a href='http://nsidc.org/pubs/notes/'>NSIDC Notes</a></li>

              <li><a href='http://nsidc.org/pubs/gd/'>Glaciological Data Series</a></li>
              <li><a href='http://nsidc.org/pubs/special/'>Special Reports</a></li>
              <li><a href='http://nsidc.org/cgi-bin/publications/pub_list.pl'>Posters &amp; Presentations</a></li>
              <li><a href='http://nsidc.org/pubs/annual/'>Annual Reports</a></li>

              <li><a href='http://nsidc.org/pubs/educationandoutreach/'>Education &amp; Outreach Materials</a></li>

            </ul>
          </li>
          <li><a class='trigger'>News & Events</a>
            <ul>
              <li><a href='http://nsidc.org/news/'>Press Room</a></li>

              <li><a href='http://nsidc.org/news/inthenews/'>NSIDC In the News</a></li>

                          <li><a href='http://nsidc.org/data/news.html'>Data News</a></li>
              <li><a href='http://nsidc.org/news/events/'>Events</a></li>
            </ul>
          </li>
          
        </ul>
        <br class='clearit'>

      </div></td>

    <td width='180' background='http://nsidc.org/ssi/images/snow_2.jpg' bgcolor='#FFFFFF'>
     <div align='right'> <a href='http://nsidc.org/cryosphere/' class='nav1'>Education Center</a>
       <a href='http://nsidc.org/gallery/' class='nav1'><br />Photo Gallery</a> 
     </div>
     <div align='right'></div></td>
  </tr>
</table>
</div>
<!-- END nsidc.ssi -->";

 echo "
<table width='100%' border='0' cellspacing='0' cellpadding='0'>
  <tr>
    <td width='71%'><div align='left'><img src='http://nsidc.org/ssi/images/glims_nav_top.jpg' width='675' height='54'>  
<td width='29%'>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor='#D6DFF7'><div align='left'><img src='http://nsidc.org/images/spacer.gif' width='1' height='1' alt='&quot;&quot;' /
    <td bgcolor='#D6DFF7'><img src='http://nsidc.org/images/spacer.gif' width='1' height='1' alt='&quot;&quot;' /></td>
  </tr>
  <tr>
    <td nowrap='nowrap' bgcolor='#00659C'>
  <td bgcolor='#00659C'>&nbsp;
    </td>
  </tr>
</table>";


?>
