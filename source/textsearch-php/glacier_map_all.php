<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<link rel="STYLESHEET" href="../css/nsidc.css" type="text/css" media="SCREEN">
</head>
<body bgcolor="#FFFFFF">
<?php 
  include '../css/nsidc_header.php';
  include 'connect.php';
  
  $current_date = date("Y-m-d h:i:s");
  $glac_id_list = $_POST['glac_ids'];  
  
  $glac_id_array = explode(",", $glac_id_list);
  $glac_ids = implode("', '", $glac_id_array);
  $glac_ids = "'" . $glac_ids . "'";
   
  #print($glac_ids);
  
  $sql          = "select box2d(collect(glacier_polys)) from glacier_query_full3 where glac_id in ($glac_ids)"; 
  $result       = pg_exec($dbh,$sql);
  $data         = pg_fetch_array($result);

  #print("<center><b>GLACIER NAME: $data[0]</b><br>");
  
  list($xy_min, $xy_max)  = split(",",$data[0]);
  list($box, $min_values) = split("\(",$xy_min);
  list($minx, $miny)      = split(" ",$min_values);
  list($maxx, $maxy)      = split(" ",$xy_max);

  # Create a new SLD file that filters the glacier layer based on the IDs returned in the search
  #1 create the new temp SLD File
  $temp_dir = "/data/tmp/";
  $pid      = getmypid();
  $SLD_file = "SLD_" . $pid . ".xml";
  $SLD_path = $temp_dir . $SLD_file;

  # Open the file for writing 
  $handle = fopen("$SLD_path", "w+");

  # Write the first line of the SLD File
  fwrite($handle, "<StyledLayerDescriptor version=\"1.0.0\">\n");

  $glac_layers = array("GLIMS_Segments","GLIMS_Points","GLIMS_Glacier_Polygons");
  $geom_layers = array("segments","points","glacier_polys");
  $colors      = array("#0002B0","#000080","#ff0000");

  # 2 Loop through the glacier ids and write an SLD Layer filter for each one
  for ($j=0; $j < count($glac_layers); $j++) {
    fwrite($handle,
      "<NamedLayer>
        <Name>$glac_layers[$j]</Name>
        <UserStyle>
         <Title></Title>
          <FeatureTypeStyle>\n");
        
    for ($i=0; $i < count($glac_id_array); $i++) {
      if($j == 1){
        fwrite($handle,
         "<Rule>
           <Filter>
            <PropertyIsEqualTo>
             <PropertyName>glac_id</PropertyName>
             <Literal>$glac_id_array[$i]</Literal>
            </PropertyIsEqualTo>
           </Filter>
           <PointSymbolizer>
            <Geometry>
             <ogc:PropertyName>$geom_layers[$j]</ogc:PropertyName>
            </Geometry>
            <Graphic>
              <Mark>
               <WellKnownName>star</WellKnownName>
               <Fill>
                <CssParameter name=\"fill\">#0000ff</CssParameter>
               </Fill>
             </Mark>
             <Size>10.0</Size>
            </Graphic>
           </PointSymbolizer>
           <LineSymbolizer>
            <Geometry>
             <PropertyName>$geom_layers[$j]</PropertyName>
            </Geometry>
            <Stroke>
             <CssParameter name=\"stroke\">$colors[$j]</CssParameter>
            </Stroke>
           </LineSymbolizer>
          </Rule>\n"); 
      } else {
        fwrite($handle,
         "<Rule>
           <Filter>
            <PropertyIsEqualTo>
             <PropertyName>glac_id</PropertyName>
             <Literal>$glac_id_array[$i]</Literal>
            </PropertyIsEqualTo>
           </Filter>
           <LineSymbolizer>
            <Geometry>
             <PropertyName>$geom_layers[$j]</PropertyName>
            </Geometry>
            <Stroke>
             <CssParameter name=\"stroke\">$colors[$j]</CssParameter>
             <CssParameter name=\"stroke-width\">2.0</CssParameter>
            </Stroke>
           </LineSymbolizer>
          </Rule>\n");
      }     
    }      
     fwrite($handle,
      "</FeatureTypeStyle>
        </UserStyle>
        </NamedLayer>\n");
  }

  # Write the last line of the SLD
  fwrite($handle, "</StyledLayerDescriptor>\n"); 
  
  
  $getmap = "http://glims.colorado.edu/cgi-bin/glims_ogc?"
          . "SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&"
          . "LAYERS=COUNTRY_BORDERS,Source_Image_Rasters,GLIMS_GLACIERS&STYLES=&"
          . "SRS=EPSG:4326&BBOX=$minx,$miny,$maxx,$maxy&"
          . "WIDTH=600&HEIGHT=400&"
          . "FORMAT=image/png&"
          . "SLD=http://glims.colorado.edu/tmp/$SLD_file";
  
  echo "<center><img src='$getmap' border='0'></center><hr>";
  
 # if ($data[2] <= $current_date){
 #   echo "<form name='download' method=POST action='../php_utils/get_data.php'>
 #          <input type='hidden' name='glac_id_list' value='$glac_id'>
 #          <center><input type=submit value='Download Glacier'></center>
 #         </form>";
#  } else {
#    
 # }
  

?>
</body>
</html>
