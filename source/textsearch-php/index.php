<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>
<title>GLIMS - General Search</title>
<script type="text/javascript" LANGUAGE='javascript'>
  function do_box_check(box_name) {
    var box_name_checked = "document.search_form."+ box_name +".checked";
    //var one_box_checked = document.search_form.box_check.value;
    box_name_checked = 'true';
    //if (box_name_checked == 'true'){
    //  document.search_form.box_check.value = 'true';
    //} else if (box_name_checked == 'false'){
    //  document.search_form.box_check.value = 'false';
    //}
  }
</script>

<script type="text/javascript" src="http://nsidc.org/global/scripts/p7exp.js"></script>
<script type="text/javascript" src="../css/clear.js"></script>

<link rel="STYLESHEET" href="../css/nsidc.css" type="text/css" media="all">
</head>
<body BGCOLOR="#FFFFFF">
<?php
//include 'db_connection.php';
include '../css/nsidc_header.php';
?>
<!--div name='intro'-->
  <center><h1>Search the Global Land Ice Measurements from Space Database</h1></center><br />
 <p> This web page provides a text-based method for searching for data in the GLIMS Glacier Database.  First select what
  parameters you want included in the search results.  Below that, indicate what fields you want to constrain by clicking the corresponding
  checkboxes.  Each glacier outline returned in a result set can be viewed or downloaded individually, or the whole result set can be downloaded as
  a set.  You have a choice of several download formats.  Data under embargo cannot be downloaded.</p>

<!--/div-->

<form name="search_form" method='GET' action="search_results.php">
<!--div name='result params'-->
<hr>
<h1>Result Parameters: </h1><br />
 <p> Please select the parameters to be included in the result set.</p>

<table>
 <tr>
  <td><input type="checkbox" name="glacier_name" value="glac_name" CHECKED>Glacier Name</td>
  <td><input type="checkbox" name="src_date" value="src_date" CHECKED>Source Date</td>
  <td><input type="checkbox" name="release_okay_date" value="release_okay_date" CHECKED>Available Date</td>
  <td><input type="hidden" name="glacier_id" value="glac_id" CHECKED></td>

 </tr>
 <tr>
  <td><input type="checkbox" name="area"  value="area">Analyst-supplied Area</td>
  <td><input type="checkbox" name="db_area"  value="db_area" CHECKED>NSIDC-calculated Area</td>
  <td><input type="checkbox" name="width"  value="width">Width</td>
  <td><input type="checkbox" name="length" value="length">Length</td>
 </tr>
 <tr>
  <td><input type="checkbox" name="wgms_id" value="wgms_id">WGMS ID</td>
  <td><input type="checkbox" name="primeclass" value="primeclass">WGMS Classification</td>
  <td><input type="hidden" name="anlys_id" value="anlys_id" CHECKED></td>
 </tr>
 <tr>
  <td><input type="checkbox" name="min_elev" value="min_elev">Min Elev.</td>
  <td><input type="checkbox" name="mean_elev" value="mean_elev">Mean Elev.</td>
  <td><input type="checkbox" name="max_elev" value="max_elev">Max Elev.</td>
 </tr>
 <tr>
  <td><input type="checkbox" name="anlst_surn" value="anlst_surn" CHECKED>Analyst's Last Name</td>
  <td><input type="checkbox" name="anlst_givn" value="anlst_givn">Analyst's First Name</td>
 </tr>
 <tr>
  <td><input type="checkbox" name="rc_id" value="rc_id">Regional Center ID</td>
  <td><input type="checkbox" name="anlst_affl" value="anlst_affl" CHECKED>Analyst's Institution</td>
  <td><input type="checkbox" name="center" value="astext(centroid(glacier_polys)) as center">Center Point</td>
 </tr>
</table>

<hr>

<h1>Search Parameters: </h1> <br />
 </p>Please click one or more checkboxes (on the left) in order to enable
     searching on that parameter.  For the text fields, you can use the '%'
     character to match any string of characters.  For example, 'arap%' would
     match 'Arapaho', leading to the record for the Arapaho Glacier.
 </p>

<table border='0'>
 <tr>
  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_static">Glacier Name</a></td>
  <td colspan='2' align='left'>&nbsp<input type='text' size='15' name='glac_name_text' value=''> (Can use '%' as wildcard.)</td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Country">Country</a></td>
  <td colspan='2'>&nbsp
   <select name='country_name_select'>
    <OPTION>Select...</OPTION>
    <?php
     include 'country_list.txt';
    ?>
   </select>
  </td>
  <td>
    <b class="emph">NOTE:</b> This may take several minutes.
  </td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_dynamic">Glacier Area</a></td>
  <td align='left'>&nbsp min:<br />&nbsp<input type='text' size='10' name='glac_area_min' value=''>km<sup>2</sup></td>
  <td align='left'>&nbsp max:<br />&nbsp<input type='text' size='10' name='glac_area_max' value=''>km<sup>2</sup></td>
 </tr>

  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_static">Glacier ID</a></td>
  <td colspan='2'>&nbsp<input type='text' size='15' name='glac_id_text' value=''></td></tr>

  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_dynamic">Analysis ID</a></td>
  <td colspan='2'>&nbsp<input type='text' size='15' name='anal_id_text' value=''></td></tr>

 <tr>
  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_static">WGMS ID</a></td>
  <td colspan='2'>&nbsp<input type='text' size='15' name='wgms_id_text' value=''></td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_dynamic">WGMS Classification</a></td>
  <td colspan='2' align='left'>&nbsp
    <select name='prime_class_select' width='100'>
     <OPTION>Select...</OPTION>
     <?php
       include 'connect.php';
       $select_valids = "SELECT * FROM primary_classification_valids";
       $valids_result = pg_exec($dbh, $select_valids);
       $valids_nrows  = pg_numrows($valids_result);
       for ($i=0;$i<$valids_nrows;$i++){
         $valids = pg_fetch_array($valids_result, $i);
         print "<OPTION value='$valids[0]'>$valids[0] -- $valids[1]</OPTION> \n";
       }
     ?>
    </select>
  </td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Regional_centers">Regional Center</a></td>
  <td colspan='2' align='center'>&nbsp
   <select name='rc_name_select'>
    <OPTION>Select...</OPTION>
    <?php
     include 'connect.php';
     // $check_submission = "SELECT DISTINCT regional_centers.rc_id, regional_centers.geog_area
     //                      FROM regional_centers
     //                      ORDER BY rc_id";
     $check_submission = "SELECT DISTINCT regional_centers.rc_id, people.affiliation, regional_centers.geog_area
                          FROM regional_centers, people
                          WHERE regional_centers.chief = people.contact_id
                          ORDER BY rc_id";
     $get_submission = pg_exec($dbh, $check_submission);
     $submission_rows = pg_numrows($get_submission);
     $submitter_array = pg_fetch_all($get_submission);
     for ($i=0; $i < $submission_rows; $i++) {
       $submitted_rc = pg_fetch_array($get_submission, $i);
       print "<OPTION value='$submitted_rc[0]'>$submitted_rc[0] - $submitted_rc[1] ($submitted_rc[2])</OPTION> \n";
     }
    ?>
   </select>
  </td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org:8080/MapsAndDocs/DB/data_dictionary.php#Submission_info">Name of data submitter</a></td>
  <td colspan='2' align='left'>&nbsp<input type='text' size='25' name='submitter_name_text' value=''> (Can use '%' as wildcard.)</td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org:8080/MapsAndDocs/DB/data_dictionary.php#Submission_analyst">Name of analyst(s)</a></td>
  <td colspan='2' align='left'>&nbsp<input type='text' size='25' name='analyst_name_text' value=''> (Can use '%' as wildcard.)</td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org:8080/MapsAndDocs/DB/data_dictionary.php#Glacier_dynamic">Glacier Width</a></td>
  <td width='10'>&nbsp min:<br />&nbsp<input type='text' size='10' name='glac_width_min' value=''>m</td>
  <td width='35'>&nbsp max:<br />&nbsp<input type='text' size='10' name='glac_width_max' value=''>m</td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_dynamic">Glacier Length</a></td>
  <td width='10' align='left'>&nbsp min:<br />&nbsp<input type='text' size='10' name='glac_length_min' value=''>m</td>
  <td width='35'>&nbsp max:<br />&nbsp<input type='text' size='10' name='glac_length_max' value=''>m</td>
 </tr>
 <tr>
  <td><a href="http://www.glims.org/MapsAndDocs/DB/data_dictionary.php#Glacier_dynamic">Glacier Elev</a></td>
  <td width='10'>&nbsp min:<br />&nbsp<input type='text' size='10' name='glac_elev_min' value=''>m</td>
  <td width='35'>&nbsp max:<br />&nbsp<input type='text' size='10' name='glac_elev_max' value=''>m</td>
 </tr>
</table>
<hr>
<input type="submit" value="Search"><input type="reset" value="Reset">
</form>
</body>
</html>
