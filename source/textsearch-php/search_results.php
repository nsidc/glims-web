<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>

<title> GLIMS Search Results </title>

<script LANGUAGE='javascript'>
</script>

<link rel="STYLESHEET" href="../css/nsidc.css" type="text/css" media="SCREEN">

<style>
  .emph {
   font-weight: bold;
  }
  .red {
   color: red;
  }
</style>
<script type="text/javascript" src="http://nsidc.org/global/scripts/p7exp.js"></script>
<script type="text/javascript" src="../css/clear.js"></script>

</head>

<body bgcolor="#FFFFFF">
<?php include '../css/nsidc_header.php'; ?>
<p align='center'><font size='+2'>Search results from the Global Land Ice Measurements from Space Glacier Database</font></p>

<table>
<?php
  include 'connect.php';

  $class_map = array();

  $select_valids = "SELECT * FROM primary_classification_valids";
  $valids_result = pg_exec($dbh, $select_valids);
  $valids_nrows  = pg_numrows($valids_result);
  for ($i=0; $i < $valids_nrows; $i++){
    $valids = pg_fetch_array($valids_result, $i);
    array_push($class_map, "$valids[0] => $valids[1]");
  }

  # map short view names to their long names
  $name_map = array(
    "glac_id"           => "Glacier ID",
    "glac_name"         => "Glacier Name",
    "area"              => "Analyst-supplied Area (km<sup>2</sup>)",
    "db_area"           => "NSIDC-calculated Area (km<sup>2</sup>)",
    "width"             => "Width (m)",
    "length"            => "Length (m)",
    "min_elev"          => "Min. Elev. (m)",
    "mean_elev"         => "Mean Elev (m)",
    "max_elev"          => "Max Elev. (m)",
    "wgms_id"           => "WGMS ID",
    "primeclass"        => "WGMS Classification",
    "src_date"          => "Source Date",
    "release_okay_date" => "Date Available",
    "anlst_surn"        => "Analyst's Last Name",
    "anlst_givn"        => "Analyst's First Name",
    "rc_id"             => "Regional Center ID",
    "anlst_affl"        => "Analyst's Institution (Regional Center)",
    "anlys_id"          => "Analysis ID",
    "astext(centroid(glacier_polys)) as center" => "Center Point");

  $current_date  = date("Y-m-d h:i:s");
  $tally         = 0;
  $all_fields    = array();
  $result_fields = array();
  $glac_id_list  = array();

  # Set up the search sql based on what search parameters are populated
  array_push($all_fields, 'glac_id',
    isset($_GET[ "glacier_name"]) ? $_GET["glacier_name"] : "",
    isset($_GET[ "anlys_id"]) ? $_GET["anlys_id"] : "",
    isset($_GET[ "area" ]) ? $_GET["area"] : "",
    isset($_GET[ "db_area" ]) ? $_GET["db_area"] : "",
    isset($_GET[ "width" ]) ? $_GET["width"] : "",
    isset($_GET[ "length" ]) ? $_GET["length"] : "",
    isset($_GET[ "min_elev" ]) ? $_GET["min_elev"] : "",
    isset($_GET[ "mean_elev" ]) ? $_GET["mean_elev"] : "",
    isset($_GET[ "max_elev" ]) ? $_GET["max_elev"] : "",
    isset($_GET[ "wgms_id" ]) ? $_GET["wgms_id"] : "",
    isset($_GET[ "primeclass" ]) ? $_GET["primeclass"] : "",
    isset($_GET[ "src_date" ]) ? $_GET["src_date"] : "",
    isset($_GET[ "release_okay_date" ]) ? $_GET["release_okay_date"] : "",
    isset($_GET[ "anlst_surn" ]) ? $_GET["anlst_surn"] : "",
    isset($_GET[ "anlst_givn" ]) ? $_GET["anlst_givn"] : "",
    isset($_GET[ "rc_id" ]) ? $_GET["rc_id"] : "",
    isset($_GET[ "anlst_affl" ]) ? $_GET["anlst_affl"] : "",
    isset($_GET[ "center" ]) ? $_GET["center"] : ""
  );

  $result_count = count($all_fields);
  for ( $i = 0; $i < $result_count; $i++ ) {
   if (!empty($all_fields[$i])) {
     array_push($result_fields, $all_fields[$i]);
   }
  }

  $count      = count($result_fields);
  $search_sql = "SELECT DISTINCT ";

  $country_name_select = isset($_GET[ 'country_name_select' ]) ? $_GET[ 'country_name_select' ] : "";

  for ( $i = 0; $i < $count; $i++ ){
    if (!empty($result_fields[$i])) {
      if ($i == 0) {
        $search_sql .= $result_fields[$i];
      }
      else {
        $search_sql .= ", " . $result_fields[$i];
      }
    }
  }

  if ($country_name_select != 'Select...') {
    $search_sql .= " FROM glacier_query_full3 gq,country WHERE gq.line_type = 'glac_bound'";
  }
  else {
    $search_sql .= " FROM glacier_query_full3 gq WHERE gq.line_type = 'glac_bound'";
  }

  # Now we add the WHERE to $search_sql (based on what the user's specified)
  print "<b>Search Parameters:</b><br />";

  if ($country_name_select != 'Select...') {
    print "Glaciers within the country: $country_name_select <br />";
    $search_sql .= " AND country.borders && gq.glacier_polys"
                 . " AND ST_intersects(gq.glacier_polys,country.borders)"
                 . " AND country.cntry_name = '$country_name_select'";
    $tally += 1;
  }


  $glac_name_text   = isset($_GET[ 'glac_name_text' ]) ? $_GET['glac_name_text'] : "";

  if (!empty($glac_name_text)) {
      print "Glacier Name: $glac_name_text <br />";
      $search_sql .= " AND gq.glac_name ILIKE '$glac_name_text'";
      $tally += 1;
  }

  $submitter_name_text   = isset($_GET[ 'submitter_name_text' ]) ? $_GET['submitter_name_text'] : "";
  if (!empty($submitter_name_text)){
    print "Submitter Name: $submitter_name_text <br />";
    $search_sql .= " AND (gq.submit_surn ILIKE '$submitter_name_text' OR gq.submit_givn ILIKE '$submitter_name_text')";
    $tally += 1;
  }

  $analyst_name_text   = isset($_GET[ 'analyst_name_text' ]) ? $_GET['analyst_name_text'] : "";
  if (!empty($analyst_name_text)){
    print "Analyst Name: $analyst_name_text <br />";
    $search_sql .= " AND (gq.anlst_surn ILIKE '$analyst_name_text' OR gq.anlst_givn ILIKE '$analyst_name_text')";
    $tally += 1;
  }

  $glac_area_min    = isset($_GET[ 'glac_area_min' ]) ? $_GET['glac_area_min'] : "";
  $glac_area_max    = isset($_GET[ 'glac_area_max' ]) ? $_GET['glac_area_max'] : "";

  if (!empty($glac_area_min) && !empty($glac_area_max)) {
    $search_sql .= " AND gq.db_area > '$glac_area_min' AND gq.db_area < '$glac_area_max'";
    print "Glaciers where: $glac_area_min km<sup>2</sup> < NSIDC-calculated Area < $glac_area_max km<sup>2</sup>";
    $tally += 1;
  } else if (empty($glac_area_min) && !empty($glac_area_max)) {
    $search_sql .= " AND gq.db_area < '$glac_area_max'";
    print "Glaciers where: NSIDC-calculated Area < $glac_area_max km<sup>2</sup>";
    $tally += 1;
  } else if (empty($glac_area_max) && !empty($glac_area_min)) {
    $search_sql .= " AND gq.db_area > '$glac_area_min'";
    print "Glaciers where: NSIDC-calculated Area > $glac_area_min km<sup>2</sup>";
    $tally += 1;
  }

  $glac_id_text   = isset($_GET[ 'glac_id_text' ]) ? $_GET['glac_id_text'] : "";

  if (!empty($glac_id_text)) {
    $search_sql .= " AND gq.glac_id ILIKE '%$glac_id_text%'";
    print "Glacier ID: $glac_id_text";
    $tally += 1;
  }

  $anal_id_text   = isset($_GET[ 'anal_id_text' ]) ? $_GET['anal_id_text'] : "";

  if (!empty($anal_id_text)){
    $search_sql .= " AND gq.anlys_id = $anal_id_text";
    print "Analysis ID: $anal_id_text";
    $tally += 1;
  }

  $wgms_id_text = isset($_GET[ 'wgms_id_text' ]) ? $_GET['wgms_id_text'] : "";

  if (!empty($wgms_id_text)) {
    $search_sql .= " AND gq.wgms_id ILIKE '%$wgms_id_text%'";
    print "WGMS ID: $wgms_id_text";
    $tally += 1;
  }


  $glac_width_min    = isset($_GET[ 'glac_width_min' ]) ? $_GET['glac_width_min'] : "";
  $glac_width_max    = isset($_GET[ 'glac_width_max' ]) ? $_GET['glac_width_max'] : "";

  if (!empty($glac_width_min) && !empty($glac_width_max)) {
    $search_sql .= " AND gq.width > '$glac_width_min' AND gq.width < '$glac_width_max'";
    print "Glaciers where:  $glac_width_min < Width < $glac_width_max";
    $tally += 1;
  } else if (empty($glac_width_min) && !empty($glac_width_max)) {
    $search_sql .= " AND gq.width < '$glac_width_max'";
    print "Glaciers where:  Width < $glac_width_max";
    $tally += 1;
  } else if (empty($glac_width_max) && !empty($glac_width_min)) {
    $search_sql .= " AND gq.width > '$glac_width_min'";
    print "Glaciers where:  Width > $glac_width_min";
    $tally += 1;
  }

  $glac_length_min    = isset($_GET[ 'glac_length_min' ]) ? $_GET['glac_length_min'] : "";
  $glac_length_max    = isset($_GET[ 'glac_length_max' ]) ? $_GET['glac_length_max'] : "";

  if (!empty($glac_length_min) && !empty($glac_length_max)) {
    $search_sql .= " AND gq.length > '$glac_length_min' AND gq.length < '$glac_length_max'";
    print "Glaciers where: $glac_length_min < Length < $glac_length_max";
    $tally += 1;
  } else if (empty($glac_length_min) && !empty($glac_length_max)) {
    $search_sql .= " AND gq.length < '$glac_length_max'";
    print "Glaciers where: Length < $glac_length_max";
    $tally += 1;
  } else if (empty($glac_length_max) && !empty($glac_length_min)) {
    $search_sql .= " AND gq.length > '$glac_length_min'";
    print "Glaciers where: Length > $glac_length_min";
    $tally += 1;
  }

  $glac_elev_min    = isset($_GET[ 'glac_elev_min' ]) ? $_GET['glac_elev_min'] : "";
  $glac_elev_max    = isset($_GET[ 'glac_elev_max' ]) ? $_GET['glac_elev_max'] : "";

  if (!empty($glac_elev_min) && !empty($glac_elev_max)) {
    $search_sql .= " AND gq.min_elev > '$glac_elev_min' AND gq.max_elev < '$glac_elev_max'";
    print "Glaciers where:  $glac_elev_min < Elevation < $glac_elev_max";
    $tally += 1;
  } else if (empty($glac_elev_min) && !empty($glac_elev_max)) {
    $search_sql .= " AND gq.max_elev < '$glac_elev_max'";
    print "Glaciers where: Elevation < $glac_elev_max";
    $tally += 1;
  } else if (empty($glac_elev_max) && !empty($glac_elev_min)) {
    $search_sql .= " AND gq.min_elev > '$glac_elev_min'";
    print "Glaciers where: Elevation > $glac_elev_min";
    $tally += 1;
  }

  $rc_name_select = isset($_GET[ 'rc_name_select' ]) ? $_GET['rc_name_select'] : "";

  if ($rc_name_select != 'Select...') {
      $search_sql .= " AND gq.rc_id = '$rc_name_select'";
      print "Glaciers provided by RC $rc_name_select";
      $tally += 1;
  }

  $prime_class_select = isset($_GET[ 'prime_class_select' ]) ? $_GET['prime_class_select'] : "";

  if ($prime_class_select != 'Select...' ) {
      $search_sql .= " AND gq.primeclass = '$prime_class_select'";
      print "Glaciers classified as $prime_class_select";
      $tally += 1;
  }

print "<br /><b><a href='javascript: history.go(-1)'>Refine your search parameters?</b></a><br />
</table>
<hr />";

  $colors = array('#FFFFFF','#C7C7C7');

  # Run the query if there were any search parameters selected.

  if ($tally == 0) {
    print "<p style='color: red;'>Please enter at least one search parameter to search the GLIMS Database.</p>";
  }
  else {
     if (isset($_GET[ "glacier_name"])){
       $search_sql .= ' ORDER BY gq.glac_name';
     } else {
       $search_sql .= ' ORDER BY gq.glac_id';
     }
    #print "<p>$search_sql</p>\n";
    $result = pg_query($dbh, $search_sql);
    $rows = pg_num_rows($result);

    print "<h2>Number of GLIMS glacier snapshots found:  $rows</h2>\n";
    print "<table align='center' border='0'>
           <tr bgcolor='#C7C7C7'><td></td>\n";
    for ( $i = 0; $i < $count; $i++ ){
      print "<td align='center' class='emph'>" . $name_map[$result_fields[$i]] . "</td>";
    }
    print  "<td align='center' bgcolor='#C7C7C7' colspan='3' class='emph'>Options</td>
         </tr>";

    if ($rows == '0'){
      print "<tr><td colspan='21' align='center'>
               <p style='color: red;'>
               No records were found that match your search criteria. Please re-define your search.
               </p></td></tr>";
    }
    else {
      $color = 0;
      for ( $i = 0; $i < $rows; $i++ ) {
        $data = pg_fetch_row($result,$i);
        print "<tr bgcolor='$colors[$color]'>";
        print "<td align='center'>$i</td>\n";

        $num_fields = count($data);
        for ($j = 0; $j < $num_fields; $j++) {
          $fieldname = pg_field_name($result, $j);
          if ($fieldname == 'anlys_id') {
            $analysis_id = $data[$j];
          }
          if ($fieldname == 'glac_id') {
            $glacier_id = $data[$j];
          }
          if ($fieldname == 'primeclass'){
            print "<td align='center'>" . $class_map[$data[$j]] . "</td>\n";
          } else if ($fieldname == 'center') {
             list($left,$right) = split(" ", $data[$j]);
             list($dum1,$lat1) = split("\(", $left);
             list($lon1,$dum2) = split("\)", $right);
             $lat = round($lon1, 4);
             $lon = round($lat1, 4);
             print "<td align='center'>$lat $lon</td>\n";
          }
          else {
            print "<td align='center'>$data[$j]</td>\n";
          }
        }
        $color = !$color;

        print "<td align='left'><a target='_blank' href='http://www.glims.org/maps/info.html?anlys_id=$analysis_id'>More info</a></td>\n";
        $release_date = pg_query($dbh, "SELECT release_okay_date FROM glacier_query_full3 where glac_id = '$glacier_id'");
        $src_date= pg_fetch_row($release_date);
        $date = $src_date[0];
        if ($current_date >= $date){
          print "<td align='left'><a href='http://www.glims.org/maps/download.html?download_type=id&clause=$glacier_id' target='_blank'>Download</a></td>\n";
        }
        else {
          print "<td align='left' class='red'>Embargoed</td>\n";
        }
        # Build Glacier IDs List for the Get Map
        array_push($glac_id_list,"$data[0]");
        $glac_ids = implode(",", $glac_id_list);
      }
    }

    print "<tr bgcolor='#C7C7C7'><td colspan='21' align='center'>End of Results</td></tr>\n
         </table>";
    print "<center>
           <a href='http://www.glims.org/maps/download.html?download_type=id&clause=$glac_ids' target='_blank'>Download available data</a>
          </center>\n";
  }
?>

</body>
</html>

