# README #

Code (mostly PHP) for the original text search interface for the GLIMS Glacier Database.  It's the list to be replaced, but is still being used.

### Set up ###

The code is deployed on the glims.colorado.edu virtual machine.  An index.php file creates the text search form, and a search_results.php file performs the query and presents the results.

### Who do I talk to? ###

* Bruce Raup
