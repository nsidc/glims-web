lookup('classes', {merge => unique}).include

# APPLICATION CONFIGURATION
# ==================================================

$app_env = $environment ? {
  /(local|integration)/ => 'integration',
  default               => $environment
}

file { '/srv/www':
  source  => '/vagrant/source',
  recurse => true,
  owner   => 'www-data',
  group   => 'www-data'
}

file_line { 'set app environment':
  path    => '/srv/www/js/config.js',
  line    => "var environment = '${app_env}';",
  match   => '^var environment = .*$',
  require => File['/srv/www']
}

file_line { 'set app user':
  path    => '/srv/www/js/config.js',
  line    => "  var user = '${provisioned_by}';",
  match   => '^  var user = .*$',
  require => File['/srv/www']
}

# LOG CONFIGURATION
# ==================================================

exec { "/vagrant/logsetup ${environment}":
  before => Service['nginx'],
  require => Package['nginx']
}

# NGINX CONFIGURATION
# ==================================================

package {'nginx':
  ensure  => present,
}

file { 'disable-nginx-default-site':
  ensure  => absent,
  path    => '/etc/nginx/sites-enabled/default',
  require => Package['nginx']
}

file { 'nginx-glims-site':
  ensure  => file,
  path    => '/etc/nginx/sites-available/glims',
  source  => '/vagrant/puppet/files/nginx/glims.conf',
  require => Package['nginx'],
  notify  => Service['nginx']
}

file { 'nginx-glims-site-enable':
  ensure  => link,
  path    => '/etc/nginx/sites-enabled/glims',
  target  => '/etc/nginx/sites-available/glims',
  require => File['nginx-glims-site']
}

service { 'nginx':
  ensure  => running,
  require => [ File['nginx-glims-site-enable'],
               File['disable-nginx-default-site'],
               File_line['set app environment']]
}
